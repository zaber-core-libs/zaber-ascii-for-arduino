# User Guide

## Contents

1. [Send Command to Device](#send-command-to-device)
    + [Save the Result](#save-the-result)
        + [Check Result for Errors](#check-result-for-errors)
        + [Get Reply Data](#get-reply-data)
        + [Get Device Status](#get-device-status)
        + [Get Other Reply Information](#get-other-reply-information)
    + [Send Command with Numeric Variable](#send-command-with-numeric-variable)
1. [Wait for Move to Finish](#wait-for-move-to-finish)
1. [Check if Move is Finished](#check-if-move-is-finished)
1. [Dealing with Alerts](#dealing-with-alerts)
    + [Alert Callback (Advanced)](#alert-callback-advanced)
1. [Receiving Multiple Responses](#receiving-multiple-responses)
1. [Additional Documentation](#additional-documentation)

----

This guide show how to use the Zaber Arduino Library to accomplish common tasks. It assumes that you have basic Arduino knowledge, and that you've been able to run a basic program from the [Quick Start Guide](./Quick%20Start.md) or the [Arduino Beginner's Guide](./Arduino%20Beginner's%20Guide.md).

_Note: The code snippets contained here cannot be run in isolation, and must be incorporated into an Arduino program such as the basic example from the [Quick Start Guide](./Quick%20Start.md)._

## Send Command to Device

The following code sends a `home` command to device `1`, axis `2`. Replace `home` with any other valid command for your Zaber device, as found in the [Protocol Manual](https://www.zaber.com/protocol-manual).

The `genericCommand()` function always waits for the device to reply, and the program will not continue until that reply is received or a timeout has occured. However, if you're not interested in the reply, you can choose to discard it by not saving the return value of the function in a variable:

``` cpp
connection.genericCommand("home", 1, 2);
```

Note that the `genericCommand()` function does not wait for the device to finish movement (see [`waitForIdle()`](#wait-for-move-to-finish)).

### Save the Result

The following code sends a `get pos` command to device `2`, which asks the device for its current position. Replace `get pos` with any other valid command for your Zaber device, as found in the [Protocol Manual](https://www.zaber.com/protocol-manual). The result of this command is saved in the `result` variable.

``` cpp
Result result = connection.genericCommand("get pos", 2);
```

#### Check Result for Errors

A `Result` object can contain either a message or an error. A `Result` object that has the error `OK` contains a message:

_It's recommended that you check every result for an error before using it, but it's not required. If you choose not to check the result for an error, the message is not guaranteed to be correct._

``` cpp
if (result.getError() == Result::OK)
{
    // Do something with the message
}
else
{
    Serial.println(result.getErrorString());
}
```

#### Get Reply Data

The reply data can be retrieved as a String, integer, or double with the following functions:

``` cpp
if (result.getError() == Result::OK) {
    Serial.println(result.getData()); // Get data as a String
    Serial.println(result.getDataInt()); // Get data as an integer
    Serial.println(result.getDataDouble()); // Get data as a double
}
```

#### Get Device Status

A reply will always include the device status:

``` cpp
if (result.getError() == Result::OK) {
    if (result.getStatus() == Result::IDLE) {
        Serial.println("Device is IDLE");
    } else if (result.getStatus() == Result::BUSY) {
        Serial.println("Device is BUSY");
    }
}
```

### Get Other Reply Information

Various other info from the reply can be retrieved:

``` cpp
if (result.getError() == Result::OK) {
    Serial.println(result.getDevice()); // Get the device number
    Serial.println(result.getAxis()); // Get the axis number
    Serial.println(result.getIsRejected() ? "Rejected" : "Not Rejected"); // Get if command was rejected
    if (result.getHasWarning())
    {
        Serial.println(result.getWarning()); // Get the device warning flag
    }
}
```

### Send Command with Numeric Variable

Sending a command that contains an integer or floating-point variable is not trivial, so there is a helper class called [`Zaber::Command`](./Zaber_Command.md) that can be used. It accepts any number of variables: The first must be text, and the rest can be more text, integers, and floating-point values. For example, here's how to set the maximum speed for device `1`, axis `1` to a variable:

``` cpp
int myMaxSpeed = 10000;
connection.genericCommand(Command("set maxspeed", myMaxSpeed), 1, 1);
```

## Wait for Move to Finish

The `genericCommand()` function waits for a reply from the device, but does *not* wait for the device to finish moving, as devices send their reply when movement starts. Here's how to send a command to device `2`, axis `1` to move to position `3600`, and then wait for the device to stop moving before doing something else:

``` cpp
connection.genericCommand("move abs 3600", 2, 1);
connection.waitUntilIdle(2, 1);
Serial.println("Done!");
```

## Check if Move is Finished

Here's how to send a command to device `4` to move to position `3600`, and then check the device and axis periodically to see if it has stopped moving:

``` cpp
connection.genericCommand("move abs 3600", 4);
while (connection.getStatus(1) == Result::BUSY) {
    Serial.print(".");
    delay(10);
}
Serial.println("Done!");
```

## Dealing with alerts

Zaber devices can send alerts at any time, generally when movement stops. Alerts must be enabled on devices with the `set comm.alert 1` command. This library discards alerts by default, but they can be checked for manually using the `waitForAlert()` function:

``` cpp
connection.genericCommand("set comm.alert 1", 1);
connection.genericCommand("move max", 1);
connection.waitForAlert();
Serial.println("done");
```

### Alert Callback (Advanced)

An alert callback function can be set up to handle all alerts received by the library. This function will be called with any alert received during a generic command, or during the `checkForAlert()` function.

``` cpp
void myAlertCallback(Result alert)
{
    if (alert.getError() == Result::OK) {
        Serial.println("Received an alert: ");
        Serial.println(alert.getText());
    }
}

void setup()
{
    // ...
    connection.setAlertCallback(myAlertCallback);
    connection.genericCommand("set comm.alert 1", 1);
    connection.genericCommand("move max", 1);
    // ...
}

void loop()
{
    // ...
    connection.checkForAlerts();
    // ...
}
```

## Receiving Multiple Responses

Some commands to Zaber devices will result in more than 1 response:

- Sending a command to all devices on a connection will result in 1 reply from each device
- Some commands sent to a single device will result in 1 reply and 1 or more info messages from that device

Using the `genericCommandMultiResponse()` function, the first reply will be in the returned result, and additional responses can be queried using the `next()` function of the result. The `next()` function returns `true` if there is another response available. This example shows how to get the state of `trigger 1` from device `3`, which will result in 1 reply and multiple info messages:

``` cpp
MultiResult result = connection.genericCommandMultiResponse("trigger print 1", 3);

if (result.getError == Result::OK) { // Check the reply for errors
    Serial.println("Reply received");

    while (result.next()) { // Run this loop while additional responses are available
        if (result.getError() == Result::OK) {
            Serial.println(result.getData()); // Print the data for each additional response
        }
    }
}
```

----

## Additional Documentation

Please consult the [README](../README.md) file.