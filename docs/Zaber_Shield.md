## class Zaber::Shield

Helper class that presents the Zaber shield as a serial port (Stream class). This class is only needed when using the Zaber shield, not with other types of serial ports.  

---

```c++
Shield::Shield (uint8_t address = ZABERSHIELD_ADDRESS_AA)
```
Creates a Shield object. 

**Parameters**
- `address` The i2c address of the shield. This is configured with jumpers on the shield, and the correct address must be provided here in order to be able to communicate with the shield. 

---

```c++
int Shield::available (void)
```
Gets number of bytes available to read from the receive buffer. 

**Returns:**
- The number of bytes available in the receive buffer. The Zaber shield has a 64-byte hardware receive buffer, and this library has a soft receive buffer. If this function returns 64 + SOFT_BUFFER_SIZE_BYTES you may not be consuming data fast enough, and data could be lost. 

---

```c++
void Shield::begin (uint32_t baudrate = 115200)
```
Begins a serial session with a specified baud rate. 

**Parameters**
- `baudrate` (Default = 115200) 

---

```c++
int Shield::peek (void)
```
Peek at the next byte in the receive buffer without consuming it. 

**Returns:**
- The next byte that will be returned by read(). A value of -1 indicates that the buffer is empty. 

---

```c++
int Shield::read (void)
```
Read a byte from the receive buffer. 

**Returns:**
- The next byte from the receive FIFO. A value of -1 indicates that the buffer is empty. 

---

```c++
size_t Shield::write (uint8_t data)
```
Write a byte to the transmit buffer. 

**Parameters**
- `data` The byte to output.

**Returns:**
- The number of bytes written (always 1). 

---

###### API documentation generated using [Doxygenmd](https://github.com/d99kris/doxygenmd)

