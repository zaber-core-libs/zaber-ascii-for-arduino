# Arduino Beginner's Guide

## Contents

1. [Hardware Setup](#hardware-setup)
    + [Jumpers](#jumpers)
    + [Connections](#connections)
1. [Arduino IDE Setup](#arduino-ide-setup)
1. [Simple Homing Example](#simple-homing-example)
    + [Including Zaber's Libraries](#including-zabers-libraries)
    + [Creating Objects](#creating-objects)
    + [Starting The Zaber Shield](#starting-the-zaber-shield)
    + [Issuing a Command to the Zaber Device](#issuing-a-command-to-the-zaber-device)
    + [Compiling and Running the Program On Your Arduino Board](#compiling-and-running-the-program-on-your-arduino-board)
1. [Intermediate LED Example](#intermediate-led-example)
    + [Setup Code](#setup-code)
    + [Receiving and Checking a Reply](#receiving-and-checking-a-reply)
    + [Setting Device Settings](#setting-device-settings)
1. [Using the Serial Monitor to Debug Your Code](#using-the-serial-monitor-to-debug-your-code)
1. [Troubleshooting](#troubleshooting)
1. [Additional Documentation](#additional-documentation)

----

This tutorial is intended for people who are new to Arduino. It will walk you through how to set up an Arduino and start writing simple programs using the Zaber ASCII Library for Arduino. If you just want to get a minimal example working on an Arduino, see the [Quick Start Guide](./Quick%20Start.md).

## Hardware Setup

Arduino boards are like tiny computers, but they can only run one program at a time. This makes them inexpensive and useful tools for prototyping, testing, or in this case, interacting with motion control devices.

Zaber devices communicate with Arduino boards via a serial connection. This means that a RS-232 serial communications port and cable is necessary to connect the Arduino to a Zaber device. Zaber recommends the [X-Series Shield](https://www.zaber.com/products/accessories/X-AS01) which lets you talk to Zaber
devices using a serial connection, and leaves the Arduino's serial port free to do other tasks such as send data back to a computer.

There are multiple Arduino boards available, and this library is compatible with the [Arduino Uno R3](https://store.arduino.cc/products/arduino-uno-rev3), [Arduino Mega R3](https://store.arduino.cc/products/arduino-mega-2560-rev3), and [Arduino Uno R4](https://store.arduino.cc/pages/uno-r4). We recommend the Arduino Uno R4 as it is the most capable and affordable device out of the three.

To complete this beginner's guide you will need:
- A Zaber device
- A power supply for the Zaber device
- A compatible Arduino board
- A USB cable to plug the Arduino into your PC
- An [X-Series Shield](https://www.zaber.com/products/accessories/X-AS01) for the Arduino
- An [X-DC data cable](https://www.zaber.com/products/accessories/X-DC02) to connect the Zaber device
  and the X-Series Shield
  * A-Series and T-Series devices will instead require an [S-XDC](https://www.zaber.com/products/accessories/S-XDC)
    or a [T-XDC](https://www.zaber.com/products/accessories/T-XDC), respectively

### Jumpers
The X-Series Shield allows you to power your Arduino from your Zaber X-Series device, which is useful when you have finished your program and want to run without being connected to the computer. For now though, it's best to power the Arduino over USB. Make sure the Power Source jumper on the X-Series Shield is in the "Vin/USB" position.

Zaber's X-Series Shield uses I2C to communicate with the Arduino. For most cases, the default address `AA` will work.  If you plan on using other I2C accessories, make sure you choose a unique I2C address for each shield.

![The X-series Shield's configuration jumpers](images/X-AS01_InitialJumperSelection.png)

### Connections
1. Connect the X-Series Shield (X-AS01) to your Arduino.
1. Connect your Zaber device to the X-Series Shield with the correct data cable.
1. Connect your Zaber device to power. Note that the Zaber Arduino Shield does not send power to a Zaber device.
1. Connect your Arduino to your computer with a USB cable. The `ON` LED should light up on the X-Series Shield.

<img src="images/ConnectDeviceShieldArduinoComputer.jpg" width="800" alt="Connecting your Zaber device, X-series Shield, Arduino and computer">

----

## Arduino IDE Setup

1. Download and install the [Arduino IDE](https://www.arduino.cc/en/Main/Software). We recommend version 2.x or later of the IDE, instead of the legacy 1.x version.
1. Open the Arduino IDE, and navigate to `Sketch -> Include Library -> Manage Libraries...`
1. In the Library Manager, search within the `Filter your search...` box for "Zaber".
1. Click the `Install` button for the `Zaber ASCII` library.

<img src="images/ArduinoLibraryManager.png" width="200" alt="Arduino Library Manager">

Once the Arduino IDE is installed and open on your computer, you should be presented with a screen similar to this:

<img src="images/ArduinoIDE.png" width="600" alt="Arduino IDE">

This is where all C++ code is written and compiled for Arduino boards. Code in the `setup()` function will run once, when the Arduino initially boots the program. Code in the `loop()` function is called repeatedly after `setup()` is done, so long as the Arduino board has power and the program has not halted execution.

Before trying to run any Arduino program, you must tell the Arduino IDE what type of Arduino you have and which port it is connected to on your computer. Refer to the
[Arduino IDE documentation](https://docs.arduino.cc/software/ide-v2/tutorials/getting-started/ide-v2-uploading-a-sketch/#uploading-a-sketch) for instructions on how to do this.

----

## Simple Homing Example

This section will show you the process of creating a simple program step by step, with explanation of the concepts. If you would prefer to jump directly to a complete program, you can open one of the provided examples in the Arduino IDE by navigating to `File -> Examples -> Zaber ASCII`, where the completed example below is available as `Basic_Homing`.

For now, let's write a simple program: When the Arduino turns on, we'll send the attached Zaber device back to its home position. As this command only needs to be issued once and not repeatedly, we can
utilize the `setup()` function in this case, leaving the `loop()` function empty.

### Including Zaber's Libraries

The first step is to include the `ZaberShield` and `ZaberConnection` libraries at the very top of your file.

In a new file in the Arduino IDE, add the following lines:
```c++
#include <ZaberShield.h>
#include <ZaberConnection.h>
```

>>>
**Common Questions**

_What is a library?_

A library is a pre-written, reusable bundle of code that you can include in your own program. Including the library allows you to use the classes, functions, and other functionality of the bundled code.
>>>

### Creating Objects

In order to use the various parts of Zaber's library, we first declare that we are using the `Zaber` namespace. If we left this out, we would have to write `Shield` as `Zaber::Shield`, `Connection` as `Zaber::Connection`, and so on for any other Zaber classes.

Next we'll define and initialize an object of the `Shield` class. This object represents communication with the Zaber X-Series Shield in our code. When constructing it, it needs to be provided with the I2C address that it will be using.

```c++
#include <ZaberShield.h>
#include <ZaberConnection.h>

using namespace Zaber; // <-- This line is new

Shield shield(ZABERSHIELD_ADDRESS_AA); // <-- This line is new
```

>>>
**Common Questions**

_What is a namespace?_

A namespace is a group of classes, functions, and other functionalities that are bundled together. In C++, a class in a namespace always needs the namespace as a prefix (ie. `Zaber::Shield`), unless the `using namespace` keywords are used with that namespace at the top of a file.

_What is an object?_

The object `shield` is an instance of the `Shield` class. This object represents all communication with the Zaber X-Series Shield in our Arduino program. We could potentially create other objects of the `Shield` class, giving them each a different name, and each would
represent communication with a different Zaber X-Series Shield. However, we usually just need the one.
>>>

The above code creates a global variable named `shield` to be an instance of the `Shield` serial port class. The name `ZABERSHIELD_ADDRESS_AA` is one of four constants that are defined in the library for this purpose, with names ending in `AA`, `AB`, `BA`, and `BB`. The first letter corresponds to the AD1 jumper position, and the second letter corresponds to the AD0 jumper position. You must use the constant that matches the jumper setting on your shield.

![The X-Series Shield's configuration jumpers](images/X-AS01_I2CAddressSelection.jpg)


Next, we need to initialize an object of the `Connection` class. When doing so, an object of a Serial port class must be passed in as a parameter, to let the `Connection` know how to talk to the Zaber device. This will usually be an object of the `Shield` class, such as the object `shield` that we previously created. If not using the Zaber X-Series Shield, you'll likely use an object of the [Serial class](https://www.arduino.cc/reference/en/language/functions/communication/serial/).

Here, we'll define the instance of the `Connection` class as a global variable named `connection`, and pass in the `shield` object.


```c++
#include <ZaberShield.h>
#include <ZaberConnection.h>

using namespace Zaber;

Shield shield(ZABERSHIELD_ADDRESS_AA);
Connection connection(shield);  // <-- This line is new
```

### Starting the Zaber Shield

Now that a `Connection` instance has been initialized, we start the `shield` object with the `begin()` function. This is required for all types of Serial ports. Note that the `Shield` class sets the baudrate automatically, while other Serial classes will require you to input a baud rate of `115200` as the first parameter to be compatible with Zaber devices:

```c++
#include <ZaberShield.h>
#include <ZaberConnection.h>

using namespace Zaber;

Shield shield(ZABERSHIELD_ADDRESS_AA);
Connection connection(shield);

void setup()                            // <-- This line is new
{                                       // <-- This line is new
  /* Initialize Zaber ASCII shield */   // <-- This line is new
  shield.begin();                       // <-- This line is new
}                                       // <-- This line is new
```

### Issuing a Command to the Zaber Device

With all the basic setup out of the way, we can now send commands to the device. The `genericCommand()` function of the `Connection` class is used to send a single command and wait for a single reply. The parameters of this function are:

- The `const String &command` is the ASCII command to be sent to the device, without the device or axis number included. Available commands and their syntax are available in the [Zaber ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands). For this example, we'll use the `home` command.
- The `uint8_t device` is the number of the device to send the command to. New devices are numbered `1`, but if you're not sure about the number of your device, connect your Zaber device to a computer and use [Zaber Launcher](https://software.zaber.com/zaber-launcher/) to get the device number.
- The `uint8_t axis` is the number of the axis to send the command to, and this is an optional parameter. If not specified, an axis number of `0` is used by default, which sends the command to all axes on a device. If you don't know the axis number of the axis you wish to control, connect your Zaber device to a computer and use [Zaber Launcher](https://software.zaber.com/zaber-launcher/) to find it.

```c++
#include <ZaberShield.h>
#include <ZaberConnection.h>

using namespace Zaber;

Shield shield(ZABERSHIELD_ADDRESS_AA);
Connection connection(shield);

void setup()
{
  /* Initialize Zaber ASCII shield */
  shield.begin();

  /* Issue a home command to device 1 */
  connection.genericCommand("home", 1); // <-- This line is new
}
```
The `genericCommand()` function waits for a response from the device and returns it. However, in the above example, we didn't do anything with the return value of this function, effectively discarding it. We'll look at how to use the response in a different example later in this article.

Note that even though `genericCommand()` waits for a response from the device, it does not wait for the device to stop moving before returning. To wait for that, use the `waitForIdle()` function in the [`Zaber::Connection`](./Zaber_Connection.md) class.

>>>
_What is a function?_

A function is a short section of code with a name. It can be run from multiple places in a program using its name, to avoid duplicating the same code many times.

_How do I know what functions are available?_

The [README](../README.md#api-reference) file contains links to API documentation for all the Zaber classes, such as [`Zaber::Connection`](./Zaber_Connection.md).
>>>

In order for our code to compile, we add the required `loop()` Arduino function and leave it empty:

```c++
#include <ZaberShield.h>
#include <ZaberConnection.h>

using namespace Zaber;

Shield shield(ZABERSHIELD_ADDRESS_AA);
Connection connection(shield);

void setup()
{
  /* Initialize Zaber ASCII shield */
  shield.begin();

  /* Issue a home command to device 1 */
  connection.genericCommand("home", 1);
}

void loop() {}// <-- This line is new
```

### Compiling and Running the Program On Your Arduino Board

You are now ready to run the program:

1. Ensure you've selected your Arduino board type and port (see the [Arduino IDE documentation](https://docs.arduino.cc/software/ide-v2/tutorials/getting-started/ide-v2-uploading-a-sketch/#uploading-a-sketch)).

1. Save the sketch and click the upload button at the top-left of the Arduino IDE: <img src="images/UploadButton.png" width="30" alt="Connecting your Zaber device, X-series Shield, Arduino and computer">

The program should run as soon as it has been uploaded, and send the home command to the Zaber device connected to your shield.

----

## Intermediate LED Example

If you'd like, you can follow along with this guide to learn about how the program is written;
alternatively, you can navigate within the Arduino IDE to `File -> Examples -> Zaber ASCII`, and click
on the `Intermediate_LED` example to load the completed code into the Arduino IDE.

In this example, we'll create a program that disables a device's LEDs when the device is at its home position. We may want to do this in order to quickly look at a device and see if it is at its home position at a glance.

### Setup Code

We'll start our program as before, creating objects of the `Shield` and `Connection` classes, and starting the shield:

```c++
#include <ZaberShield.h>
#include <ZaberConnection.h>

using namespace Zaber;

Shield shield(ZABERSHIELD_ADDRESS_AA);
Connection connection(shield);

void setup()
{
  /* Initialize Zaber shield */
  shield.begin();
}

void loop() {

}
```

### Receiving and Checking a Reply

Next, we'll query for the device's position in the loop function. First, send the request for the position of the device. and save the return value as a `Result` object.

```c++
#include <ZaberShield.h>
#include <ZaberConnection.h>

using namespace Zaber;

Shield shield(ZABERSHIELD_ADDRESS_AA);
Connection connection(shield);

void setup()
{
  shield.begin();
}

void loop()
{
  /* Issue a get position command to the device and save the result */
  Result result = connection.genericCommand("get pos", 1);   // <-- This line is new
}
```

Next, we'll need to check the result to see if we received an `OK` reply. This can be done with the `getError()` function of the result:

```c++
#include <ZaberShield.h>
#include <ZaberConnection.h>

using namespace Zaber;

Shield shield(ZABERSHIELD_ADDRESS_AA);
Connection connection(shield);

void setup()
{
  shield.begin();
}

void loop()
{
  /* Issue a get position command to the device and save the result */
  Result result = connection.genericCommand("get pos", 1);

  /* Check that the result of the response is OK */
  if (result.getError() == Result::OK)                // <-- This line is new
  {                                                   // <-- This line is new
    // We can do something here if the result is OK   // <-- This line is new
  }                                                   // <-- This line is new
}
```

>>>
**Common Questions**

_How can I know all the possible errors of a `Result`?_

Consult the [`Zaber::Result` API documentation](./Zaber_Result.md) for a list of all the different errors that a result can have.
>>>

Once we've confirmed that the result of a command is `OK`, we can get different parts of the reply from this same `result` object. We're only interested in the data of the reply, which will give us the device's position. Since we know the data will be an integer, we can use the `getDataInt()` function of the `Result` class:

```c++
#include <ZaberShield.h>
#include <ZaberConnection.h>

using namespace Zaber;

Shield shield(ZABERSHIELD_ADDRESS_AA);
Connection connection(shield);

void setup()
{
  shield.begin();
}

void loop()
{
  /* Issue a get position command to the device and save the result */
  Result result = connection.genericCommand("get pos", 1);

  /* Check that the result of the response is OK */
  if (result.getError() == Result::OK)
  {
    if (result.getDataInt() == 0)                               // <-- This line is new
    {                                                           // <-- This line is new
      // We can do something here if the device is at home      // <-- This line is new
    }                                                           // <-- This line is new
    else                                                        // <-- This line is new
    {                                                           // <-- This line is new
      // We can do something here if the device is not at home  // <-- This line is new
    }                                                           // <-- This line is new
  }
}
```

>>>
**Common Questions**

_How can I know all the functions and fields available for `Result`?_

Consult the [`Zaber::Result` API documentation](./Zaber_Result.md) for a list of all the functions available, including documentation on how to use them.
>>>

### Setting Device Settings

Finally, we can issue a command to change the settings on the device to turn the LED light off if the device is in the home position, and to turn the LED light on if the device is not in the home position. Note that we don't save the `Result` returned from either call to the `genericCommand()` function, although we could if we wanted to check if the command was received OK.

```c++
#include <ZaberShield.h>
#include <ZaberConnection.h>

using namespace Zaber;

Shield shield(ZABERSHIELD_ADDRESS_AA);
Connection connection(shield);

void setup()
{
  shield.begin();
}

void loop()
{
  /* Issue a get position command to the device and save the result */
  Result result = connection.genericCommand("get pos", 1);

  /* Check that the result of the response is OK */
  if (result.getError() == Result::OK)
  {
    if (result.getDataInt() == 0)
    {
       /* Set the device's LED lights to OFF if it is at home */
      connection.genericCommand("set system.led.enable 0", 1);  // <-- This line is new
    }
    else
    {
      /* Set the device's LED lights to ON if it is not at home*/
      connection.genericCommand("set system.led.enable 1", 1);  // <-- This line is new
    }
  }
}
```

You can now upload this program to your Arduino, and you'll notice that when the device is at the home
position, it will have its power LED lights and communication LED lights turned off. If you move the device
away from the home position (using the knob on the device) you'll see the LED lights turn back on, ready to be
monitored.

----

## Using the Serial Monitor to Debug Your Code

One of the advantages of using the X-Series Shield to connect your Arduino to a Zaber device is that the X-Series Shield does not use the serial port on the Arduino. This allows you to use the Arduino's serial port for something else, such as sending data back to your computer and monitoring it with the serial monitor. Here's how to expand the previous example to do this:

```c++
#include <ZaberShield.h>
#include <ZaberConnection.h>

using namespace Zaber;

Shield shield(ZABERSHIELD_ADDRESS_AA);
Connection connection(shield);

void setup()
{
  /* Initialize Zaber shield */
  shield.begin();

  Serial.begin(9600);                                             // <-- This line is new

  /*
    Wait for a USB connection to be established to a PC.
  */
  while (!Serial)                                                // <-- This line is new
  {                                                              // <-- This line is new
  }                                                              // <-- This line is new
  delay(100);                                                    // <-- This line is new
}

void loop()
{
  /* Issue a get position command to the device and save the result */
  Result result = connection.genericCommand("get pos", 1);

  /* Check that the result of the response is OK */
  if (result.getError() == Result::OK)
  {
    if (result.getDataInt() == 0)
    {
      /* Set the device's LED lights to OFF if it is at home */
      connection.genericCommand("set system.led.enable 0", 1);

      Serial.println("Position: 0 (homed) - LED should be OFF");  // <-- This line is new
    }
    else
    {
      /* Set the device's LED lights to ON */
      connection.genericCommand("set system.led.enable 1", 1);

      Serial.print("Position: ");                                 // <-- This line is new
      Serial.print(result.getDataInt());                          // <-- This line is new
      Serial.println(" - LED should be ON");                      // <-- This line is new
    }
  }

  // Add a delay to limit polling to 2 times per second
  delay(500);                                                     // <-- This line is new
}
```

Upload the program to your Arduino and open the Serial Monitor in the Arduino IDE by clicking `Tools -> Serial Monitor`. Make sure your baud rate is set to 9600 to match the code.  Assuming your serial port is still
configured properly in the IDE, you should see printouts similar to the one below, updating about twice per second.

![Serial Monitor output](images/ArduinoIDESerialMonitor.png)

---

## Troubleshooting

**The program won't upload to the Arduino board**

- Check your [Port and Board settings](https://docs.arduino.cc/software/ide-v2/tutorials/getting-started/ide-v2-uploading-a-sketch/#uploading-a-sketch).
- If you are using the Zaber X-Series Shield, ensure that the shield is powered. The ON led in the bottom
right corner should be lit. Make sure your Power Source jumper agrees with your wiring.
- If you are not using the Zaber X-Series shield and instead connecting the Zaber device to the Arduino serial port using an RS-232 adapter, make sure the adapter is turned off or disconnected during upload. The serial port on the Arduino is required to be free for program upload.
- Ensure you are using a USB cable with data wires. The USB cable that came with your Arduino board should work, but many USB cables included with consumer devices may only be designed for charging and may not have data wires for communication.

**The device isn't recognizing my commands or seems to be moving/reacting randomly**

- Make sure that you are using the correct protocol. By default, X-Series devices use the ASCII protocol, and T- and A-series devices use the Binary protocol. You can check the protocol in [Zaber Console](https://www.zaber.com/w/Software/Zaber_Console), and switch between the two protocols using the options tab.

**These fixes didn't work for me and my program still won't run**

Contact Zaber support at [contact@zaber.com](mailto:contact@zaber.com), detailing your Arduino board type,
Zaber device model, Zaber or RS-232 shield type, and the Arduino code that refuses to run.

----

## Additional Documentation

Please consult the [README](../README.md) file.
