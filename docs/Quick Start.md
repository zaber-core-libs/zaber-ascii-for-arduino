# Quick Start Guide

## Contents

1. [Install](#install)
1. [Configure](#configure)
1. [Connect](#connect)
1. [Compile & Run](#compile--run)
1. [Additional Documentation](#additional-documentation)

----

This guide is intended to get users up and running quickly with this library by installing it, configuring a device, connecting to a device, and running a simple code example.

_This guide assumes that you are using the [Zaber X-series Shield](https://www.zaber.com/products/accessories/X-AS01) to connect the Arduino to one or more Zaber devices._

## Install

1. Download and install the [Arduino IDE](https://www.arduino.cc/en/Main/Software). We recommend version 2.x or newer of the IDE, instead of the legacy 1.x version.
1. Open the Arduino IDE, and navigate to `Sketch -> Include Library -> Manage Libraries...`
1. In the Library Manager, search within the `Filter your search...` box for "Zaber".
1. Click the `Install` button for the `Zaber ASCII` library.

<img src="images/ArduinoLibraryManager.png" width="200" alt="Arduino Library Manager">

## Configure

![The X-series Shield's configuration jumpers](images/X-AS01_InitialJumperSelection.png)

1. On your [X-series Shield](https://www.zaber.com/products/accessories/X-AS01), put the Power Source jumper in the `Vin/USB` position. This ensures that the shield is powered by the Arduino, and that the Zaber device does not try to power the shield.
1. Choose address `AA` with the I2C Address jumpers.

## Connect

<img src="images/ConnectDeviceShieldArduinoComputer.jpg" width="800" alt="Connecting your Zaber device, X-series Shield, Arduino and computer">

1. Connect the X-series Shield (X-AS01) to your Arduino. Don't connect other shields just yet.
1. Connect your Zaber device to the X-series Shield with a data cable:
   [X-DC](https://www.zaber.com/products/accessories/X-DC02) for X-series,
   [S-XDC](https://www.zaber.com/products/accessories/S-XDC) for A-series, and
   [T-XDC](https://www.zaber.com/products/accessories/T-XDC) for T-series devices.
1. Connect your Zaber device to power. Note that the Zaber Arduino Shield does not send power to a Zaber device.
1. Connect your Arduino to your computer with a USB cable. The `ON` LED should light up on the X-series Shield.

## Compile & Run

Example code:

```c++
#include <ZaberConnection.h>
#include <ZaberShield.h>

using namespace Zaber;

/* Create a shield object to control the Zaber Arduino Shield */
Shield shield(ZABERSHIELD_ADDRESS_AA);

/* Create a connection object to control a connection using the shield */
Connection connection(shield);

void setup()
{
  /* Initialize Zaber ASCII shield */
  shield.begin();

  /* Issue a home command to device 1 */
  connection.genericCommand("home", 1);

  /* Wait for the device to finish moving. */
  connection.waitUntilIdle(1);

  /* Additional commands can now be issued. */
}

void loop() {

}
```

1. When you are ready to begin programming, open the Arduino IDE and paste the example code from above. This example will home a device upon startup.

1. Select your Arduino board type and port (see the [Arduino IDE documentation](https://docs.arduino.cc/software/ide-v2/tutorials/getting-started/ide-v2-uploading-a-sketch/#uploading-a-sketch)).

1. Save the sketch and click the upload button at the top-left of the Arduino IDE: <img src="images/UploadButton.png" width="30" alt="Connecting your Zaber device, X-series Shield, Arduino and computer">

1. When the upload finishes, the device should begin moving to the home position.

----

## Additional Documentation

Please consult the [README](../README.md) file.
