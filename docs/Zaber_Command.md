## class Zaber::Command

Command for a Zaber device.  

---

```c++
Command (const char * command)
```
Creates a command based on text. 

**Parameters**
- `command` Text command 

---

```c++
template<typename... Args> Command (const char * command, Args... data)
```
Creates a command based on text and additional data. 

**Parameters**
- `command` Text command 
- `data` Additional data (one or more text or numbers) 

---

```c++
operator const String & ()
```

**Returns:**
- Reference to underlying String when implicitly cast as String reference. 

---

###### API documentation generated using [Doxygenmd](https://github.com/d99kris/doxygenmd)

