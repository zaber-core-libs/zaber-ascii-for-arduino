## class Zaber::MultiResult

The MultiResult of sending a command to a Zaber device, which has the ability to check for additional responses in a multi-response chain.  

---

```c++
MultiResult::MultiResult (Result result, Connection & connection, uint8_t id, bool allDevices)
```
Initializes a MultiResult object that holds a message, and can receive future messages from the connection as part of a multi-response chain. 

**Parameters**
- `result` The Result of the first reply from the device. 
- `connection` Reference to the connection that initiated the request. 
- `id` The ID of the initial request. 
- `allDevices` True if initial command was sent to all devices, false if sent to one device. 

---

```c++
bool MultiResult::next ()
```
Check if there is another response in a multi-response chain. Checks for the next response that has an ID matching the initial command. If there is another matching response, this object will be updated with its data.

**Returns:**
- True if there is another response, false if not. 

---

###### API documentation generated using [Doxygenmd](https://github.com/d99kris/doxygenmd)

