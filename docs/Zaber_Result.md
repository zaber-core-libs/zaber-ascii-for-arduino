## class Zaber::Result

The result of sending a command to a Zaber device, which contains either a message or an error status.  

---

```c++
Result::Result (ErrorType error)
```
Creates a Result with the given error. 

**Parameters**
- `error` 

---

```c++
Result::Result (String text)
```
Creates a Result with the given message text. 

**Parameters**
- `text` 

---

```c++
uint8_t Result::getAxis ()
```

**Returns:**
- The axis that the message in this Result is from. Device messages not from a specific axis will return 0. 

---

```c++
int16_t Result::getChecksum ()
```

**Returns:**
- Checksum for the message in this Result. Returns -1 if there is no checksum. 

---

```c++
String Result::getData ()
```

**Returns:**
- Data for the message in this Result. 

---

```c++
double Result::getDataDouble ()
```

**Returns:**
- Data for the message in this Result, in the form of an double. Returns 0.0 if the data cannot be formatted as a double. 

---

```c++
int32_t Result::getDataInt ()
```

**Returns:**
- Data for the message in this Result, in the form of an integer. Returns -1 if the data cannot be formatted as an integer. 

---

```c++
uint8_t Result::getDevice ()
```

**Returns:**
- The device that the message in this Result is from. 

---

```c++
Result::ErrorType Result::getError ()
```

**Returns:**
- The error of this Result:
- OK - This Result contains a message.
- DEVICE_UNSPECIFIED - The action could not be performed because the device was not specified.
- TIMEOUT - A timeout occured while waiting for a message.
- MESSAGE_INVALID - A message was received, but it was invalid.
- COMMAND_REJECTED - The device responded and rejected the command.
- NO_MESSAGE - There was no message available. 

---

```c++
String Result::getErrorString ()
```

**Returns:**
- String description of the error of this Result. 

---

```c++
bool Result::getHasWarning ()
```

**Returns:**
- True if the message in this Result has a warning flag, false if it does not. Note that info messages don't have warning flags. 

---

```c++
int8_t Result::getId ()
```

**Returns:**
- The id of the message in this Result from 0 to 99. Returns -1 if message is not a reply or info. 

---

```c++
bool Result::getIsRejected ()
```

**Returns:**
- True if the message in this Result is a reply and is rejected, false if message is not rejected or message is not a reply. 

---

```c++
Result::IdleStatus Result::getStatus ()
```

**Returns:**
- Status of the message in this Result, which indicates IDLE or BUSY. Returns ERROR for info messages. 

---

```c++
String Result::getText ()
```

**Returns:**
- The full text of the message in this Result. 

---

```c++
Result::MessageType Result::getType ()
```

**Returns:**
- The type of the message in this Result. EMPTY indicates no message, check the result's error for more details. 

---

```c++
String Result::getWarning ()
```

**Returns:**
- Warning flag for a message in this Result. Returns '--' if there is no warning, or if it is an info message. 

---

###### API documentation generated using [Doxygenmd](https://github.com/d99kris/doxygenmd)

