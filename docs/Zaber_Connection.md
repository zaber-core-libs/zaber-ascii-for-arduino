## class Zaber::Connection

Connection to one or more Zaber devices.  

---

```c++
Connection::Connection (SerialPort & serial)
```
Initializes a connection with the specified serial port. 

**Parameters**
- `serial` 

---

```c++
void Connection::checkAlerts ()
```
Check for alerts received and notify the alert callback. Checks the receive buffer for incoming alert messages from Zaber devices. If an alert callback has been set and the message received is an alert, calls the alert callback with this alert. If message is reply or info, discards the message. Continues until receive buffer is empty.
To get notified about alerts when calling this function, the alert callback must be set first using the setAlertCallback() function. If you wish to wait for an alert instead, use the waitForAlert() function. 

---

```c++
void Connection::disableChecksum ()
```
Disable checksums for outgoing commands. Sending checksums for outgoing commands will request checksums in responses, ensuring message integrity. By default, checksums are enabled. 

---

```c++
void Connection::enableChecksum ()
```
Enable checksums for outgoing commands. Sending checksums for outgoing commands will request checksums in responses, ensuring message integrity. By default, checksums are enabled. 

---

```c++
Result Connection::genericCommand (const String & command, uint8_t device, uint8_t axis = 0)
```
Sends a generic command over the connection and waits for a single response. Command is sent to a specific device or axis. If a response is not received within the connection's timeout, a Result with a TIMEOUT error is returned. This function does not wait for the device to finish moving before returning (see waitForIdle() function). Any alerts received while waiting for a response notify the alert callback, if it has been set.

**Parameters**
- `command` The command to issue to the device. To add numeric data, see ZaberCommand.h. 
- `device` The device to send the command to. Must be >= 1. 
- `axis` The axis to send the command to. A value of 0 sends command to all axes of the device. (Default = 0)

**Returns:**
- Result of sending the generic command, which can contain a message or an error. 

---

```c++
MultiResult Connection::genericCommandMultiResponse (const String & command, uint8_t device = 0, uint8_t axis = 0)
```
Sends a generic command over the connection and expects multiple responses. Commands can be sent to a specific device or axis, or to all devices. Waits up to the connection's timeout for the first response. This function does not wait for the device to finish moving before returning (see waitForIdle() function). Additional responses can be checked using the next() function of the MultiResult (see ZaberMultiResult.h). Any alerts received while waiting for a response notify the alert callback, if it has been set.

**Parameters**
- `command` The command to issue to the device. To add numeric data, see ZaberCommand.h. 
- `device` The device to send the command to. A value of 0 sends command to all devices on the chain. (Default = 0) 
- `axis` The axis to send the command to. A value of 0 sends command to all axes of the device. (Default = 0)

**Returns:**
- MultiResult for sending the command which contains the first response or an error, and can be checked for more responses. 

---

```c++
uint8_t Connection::genericCommandNoResponse (const String & command, uint8_t device = 0, uint8_t axis = 0, uint8_t id = 255)
```
Sends a generic command over the connection. Sends a command and does not check the receiving buffer for a response. This function does not wait for the device to finish moving before returning (see waitForIdle() function). This function must be followed by one or more calls to waitForResponse(), otherwise the receive buffer may overflow. The genericCommand() and genericCommandMultiResponse() functions are recommended over this function.

**Parameters**
- `command` The command to issue to the device. To add numeric data, see ZaberCommand.h. 
- `device` The device to send the command to. A value of 0 sends command to all devices on the chain. (Default = 0) 
- `axis` The axis to send the command to. A value of 0 sends command to all axes of the device. (Default = 0) 
- `id` Custom message ID for this command. IDs > 99 are invalid and will be ignored. (Default = 255)

**Returns:**
- The ID of the sent command. 

---

```c++
Result::IdleStatus Connection::getStatus (uint8_t device, uint8_t axis = 0)
```
Check whether a device/axis is currently idle or moving. 

**Parameters**
- `device` The device number to check. Must be >= 1 or this function will return an ERROR IdleStatus. 
- `axis` The axis number to check. A value of 0 checks the device only. (Default = 0)

**Returns:**
- An IdleStatus which will be either IDLE, BUSY, or ERROR. 

---

```c++
void Connection::setAlertCallback (AlertCallbackPtr callback)
```
Set a callback function to handle alerts. 

**Parameters**
- `callback` A callback function pointer of the type 'void func(Result)' that handles incoming alerts from Zaber devices. This callback may be triggered when calling checkAlerts() or when waiting for a response. 

---

```c++
void Connection::setTimeout (uint32_t timeout)
```
Set the time to wait for a response from the connection. By default, this value is 100ms.

**Parameters**
- `timeout` The time in milliseconds 

---

```c++
Result Connection::waitForAlert (uint32_t timeout = 10000)
```
Wait for an alert message. Attempts to read an alert message from the connection. If an alert is not received by the timeout, a Result with a TIMEOUT error is returned. Any reply or info messages received during this function are discarded.

**Parameters**
- `timeout` The amount of time to wait for an alert message in milliseconds. Note that this timeout is different than the connection timeout. (Default = 10000)

**Returns:**
- A Result, which can contain a message or an error. 

---

```c++
Result Connection::waitForResponse ()
```
Wait for a response message. Attempts to read a response (reply or info) from the connection. If a response is not received by the connection's timeout, a Result with a TIMEOUT error is returned. Any alerts received while waiting for a response notify the alert callback, if it has been set.

**Returns:**
- A Result, which can contain a message or an error. 

---

```c++
Result Connection::waitForResponse (uint8_t id)
```
Wait for a response message. Attempts to read a response (reply or info) message from the connection with the given message ID. If a matching response is not received by the connection's timeout, a Result with a TIMEOUT error is returned. Any alerts received while waiting for a response notify the alert callback, if it has been set.

**Parameters**
- `id` The message ID to wait for. Must be >= 0 and <= 99.

**Returns:**
- A Result, which can contain a message or an error. 

---

```c++
Result Connection::waitUntilIdle (uint8_t device, uint8_t axis = 0, uint32_t timeout = 0)
```
Wait until the device has completed movement. This function sends out regular polls and blocks execution until the device responds with an idle status or an alert is received with idle status. By default, this function waits indefinitely. If the timeout is > 0, this function will only wait until the timeout, and then return with a TIMEOUT error if the device is still busy.

**Parameters**
- `device` The device number to wait for. Must be >= 1 or this function will return immediately with a DEVICE_UNSPECIFIED error. 
- `axis` The axis number to wait for. A value of 0 polls the device only. (Default = 0) 
- `timeout` The amount of time in milliseconds to wait until returning a Result with a TIMEOUT error. A value of 0 will cause this function to wait indefinitely. Note that this timeout is different than the connection timeout. (Default = 0)

**Returns:**
- Result of the wait, which is either a message containing IDLE, or a TIMEOUT error. 

---

###### API documentation generated using [Doxygenmd](https://github.com/d99kris/doxygenmd)

