#include "ZaberMultiResult.h"
#include "ZaberConnection.h"

using namespace Zaber;

MultiResult::MultiResult(Result result, Connection &connection, uint8_t id, bool allDevices)
    : Result(result), _connection(connection), _id(id), _allDevices(allDevices)
{
}

bool MultiResult::next()
{
  Result result = _connection.waitForResponse(_id);
  /* If next message is a reply and the initial command was sent to a specific device, the
   * multi-response chain is over. */
  if (result.getError() == Result::OK && result.getType() == Result::REPLY && !_allDevices)
  {
    return false;
  }
  else if (result.getError() == Result::TIMEOUT)
  {
    return false;
  }
  else
  {
    _error = result.getError();
    _type = result.getType();
    _text = result.getText();
    return true;
  }
}
