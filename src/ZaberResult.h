#ifndef _ZABER_RESULT_H_
#define _ZABER_RESULT_H_

#include <Arduino.h>

namespace Zaber
{

/**
 * @brief The result of sending a command to a Zaber device, which contains either a message or an
 * error status.
 */
class Result
{
  public:
  /**
   * @brief Type of result error.
   */
  enum ErrorType
  {
    OK = 0,
    DEVICE_UNSPECIFIED,
    TIMEOUT,
    MESSAGE_INVALID,
    COMMAND_REJECTED,
    NO_MESSAGE,
  };

  /**
   * @brief Type of result message.
   */
  enum MessageType
  {
    EMPTY = 0,
    REPLY,
    INFO,
    ALERT,
  };

  /**
   * @brief Idle status of message.
   */
  enum IdleStatus
  {
    ERROR = -1,
    IDLE = 0,
    BUSY = 1,
  };

  /**
   * @brief Creates a Result with the given error.
   *
   * @param error
   */
  Result(ErrorType error);

  /**
   * @brief Creates a Result with the given message text.
   *
   * @param text
   */
  Result(String text);

  /**
   * @return The error of this Result:
   *
   * OK - This Result contains a message.
   *
   * DEVICE_UNSPECIFIED - The action could not be performed because the device was not specified.
   *
   * TIMEOUT - A timeout occured while waiting for a message.
   *
   * MESSAGE_INVALID - A message was received, but it was invalid.
   *
   * COMMAND_REJECTED - The device responded and rejected the command.
   *
   * NO_MESSAGE - There was no message available.
   * */
  ErrorType getError();

  /**
   * @return String description of the error of this Result.
   */
  String getErrorString();

  /**
   * @return The type of the message in this Result. EMPTY indicates no message, check the result's
   * error for more details.
   */
  MessageType getType();

  /**
   * @return The full text of the message in this Result.
   */
  String getText();

  /**
   * @return The device that the message in this Result is from.
   */
  uint8_t getDevice();

  /**
   * @return The axis that the message in this Result is from. Device messages not from a specific
   * axis will return 0.
   */
  uint8_t getAxis();

  /**
   * @return The id of the message in this Result from 0 to 99. Returns -1 if message is not a reply
   * or info.
   */
  int8_t getId();

  /**
   * @return True if the message in this Result is a reply and is rejected, false if message is not
   * rejected or message is not a reply.
   */
  bool getIsRejected();

  /**
   * @return Status of the message in this Result, which indicates IDLE or BUSY. Returns ERROR for
   * info messages.
   */
  IdleStatus getStatus();

  /**
   * @return True if the message in this Result has a warning flag, false if it does not. Note that
   * info messages don't have warning flags.
   */
  bool getHasWarning();

  /**
   * @return Warning flag for a message in this Result. Returns "--" if there is no warning, or if
   * it is an info message.
   */
  String getWarning();

  /**
   * @return Data for the message in this Result.
   */
  String getData();

  /**
   * @return Data for the message in this Result, in the form of an integer. Returns -1 if the data
   * cannot be formatted as an integer.
   */
  int32_t getDataInt();

  /**
   * @return Data for the message in this Result, in the form of an double. Returns 0.0 if the data
   * cannot be formatted as a double.
   */
  double getDataDouble();

  /**
   * @return Checksum for the message in this Result. Returns -1 if there is no checksum.
   */
  int16_t getChecksum();

  protected:
  ErrorType _error = OK;
  MessageType _type = EMPTY;
  String _text = "";
};

} // namespace Zaber

#endif /* _ZABER_RESULT_H_ */