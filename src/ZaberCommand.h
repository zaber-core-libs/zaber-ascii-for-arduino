#ifndef _ZABER_COMMAND_H_
#define _ZABER_COMMAND_H_

#include <Arduino.h>

namespace Zaber
{

/**
 * @brief Command for a Zaber device
 */
class Command
{
  public:
  /**
   * @brief Creates a command based on text
   *
   * @param command Text command
   */
  Command(const char *command) : _command(command) {}

  /**
   * @brief Creates a command based on text and additional data
   *
   * @param command Text command
   * @param data    Additional data (one or more text or numbers)
   */
  template <typename... Args> Command(const char *command, Args... data) : _command(command)
  {
    processArgs(data...);
  }

  /**
   * @return Reference to underlying String when implicitly cast as String reference.
   */
  operator const String &() { return _command; }

  private:
  template <typename T>
  void processArgs(T data)
  {
    _command.concat(" ");
    _command.concat(data);
  }

  template <typename T, typename... Args>
  void processArgs(T data, Args... args)
  {
    _command.concat(" ");
    _command.concat(data);
    processArgs(args...);
  }

  String _command;
};

} // namespace Zaber

#endif /* _ZABER_COMMAND_H_ */