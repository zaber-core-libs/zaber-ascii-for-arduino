#ifndef _ZABER_MULTI_RESULT_H_
#define _ZABER_MULTI_RESULT_H_

#include "ZaberResult.h"

namespace Zaber
{
/* Forward Declaration */
class Connection;

/**
 * @brief The MultiResult of sending a command to a Zaber device, which has the ability to check for
 * additional responses in a multi-response chain.
 */
class MultiResult : public Result
{
  public:
  /**
   * @brief Initializes a MultiResult object that holds a message, and can receive future messages
   * from the connection as part of a multi-response chain.
   *
   * @param result     The Result of the first reply from the device.
   * @param connection Reference to the connection that initiated the request.
   * @param id         The ID of the initial request.
   * @param allDevices True if initial command was sent to all devices, false if sent to one device.
   */
  MultiResult(Result result, Connection &connection, uint8_t id, bool allDevices);

  /**
   * @brief Check if there is another response in a multi-response chain.
   *
   * Checks for the next response that has an ID matching the initial command. If there is another
   * matching response, this object will be updated with its data.
   *
   * @return True if there is another response, false if not.
   */
  bool next();

  private:
  Connection &_connection;
  uint8_t _id;
  bool _allDevices;
};

} // namespace Zaber

#endif /* _ZABER_MULTI_RESULT_H_ */