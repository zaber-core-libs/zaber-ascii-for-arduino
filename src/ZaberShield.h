/*
  Description: This class handles I2C communication with the SC16IS740 UART
  bridge chip, as found on the Zaber Arduino shield, and makes the shield appear
  as a serial port to your code. You can use this class as a substitute for
  the regular Serial class when using the Zaber Arduino library with the
  Zaber Arduino shield. It may also work with other shields that use the same
  chip.

  This code is derived from Sandbox Electronics' UART_Bridge example code by
  Tiequan Shao, and is adapted and redistributed under the Apache 2 license
  with their kind permission. The original code (which also works with the
  Zaber shield) is available here: https://github.com/SandboxElectronics/UART_Bridge

  Note the original Sandbox implementation supports both I2C and SPI to serial
  bridging, ann the GPIO pins of the SC16IS750 chip. This modified Zaber version
  only supports I2C because the Zaber shield only uses an I2C connection and
  does not have the GPIO functionality. If you are also using an SPI connection
  on another shield, consider using the Sandbox library instead of this class.

  The license statement provided by Sandbox Electronics appears below:
  ---------------------------

  Description:
  This is a example code for Sandbox Electronics' I2C/SPI to UART bridge module.
  You can get one of those products on
  http://sandboxelectronics.com

  Version:
  V0.1

  Release Date:
  2014-02-16

  Author:
  Tiequan Shao          info@sandboxelectronics.com

  Lisence:
  Apache License Version 2.0

  Please keep the above information when you use this code in your project.
*/

#ifndef _ZABERSHIELD_H_
#define _ZABERSHIELD_H_

/* Prevent multiple definition of this class if using both the ASCII and Binary Zaber libraries. */
#define _USING_ZABERSHIELD_IN_ASCII_LIBRARY_

#include <Arduino.h>

/*
  Size of the software receive buffer. If more than a single message is received at once from the
  hardware buffer, everything after the first message is stored in here. Define this to a maximum
  of 64 to reduce the chance of lost messages, or to a minimum of 4 to reduce memory footprint.
 */
#ifndef SOFT_BUFFER_SIZE_BYTES
#define SOFT_BUFFER_SIZE_BYTES 32
#endif /* SOFT_BUFFER_SIZE_BYTES */

/*
  The i2c address of the UART bridge chip on the Zaber shield is configured with two jumpers on the
  shield. The address you use in your sketch with the ZaberShield class must match the jumper
  settings. Note that if you change the jumpers on the shield, a power cycle is needed for the
  shield to pick up the new address.
*/
#define ZABERSHIELD_ADDRESS_AA (0x90)
#define ZABERSHIELD_ADDRESS_AB (0x92)
#define ZABERSHIELD_ADDRESS_BA (0x98)
#define ZABERSHIELD_ADDRESS_BB (0x9A)

namespace Zaber
{

/**
 * @brief Helper class that presents the Zaber shield as a serial port (Stream class). This class is
 * only needed when using the Zaber shield, not with other types of serial ports.
 */
class Shield : public Stream
{
  public:
  /**
   * @brief Creates a Shield object.
   *
   * @param address The i2c address of the shield. This is configured with jumpers on the shield,
   * and the correct address must be provided here in order to be able to communicate with the
   * shield.
   */
  Shield(uint8_t address = ZABERSHIELD_ADDRESS_AA);

  /**
   * @brief Begins a serial session with a specified baud rate.
   *
   * @param baudrate (Default = 115200)
   */
  void begin(uint32_t baudrate = 115200);

  /**
   * @brief Ends the serial session.
   */
  void end();

  /**
   * @brief Gets number of bytes available to read from the receive buffer.
   *
   * @return The number of bytes available in the receive buffer. The Zaber shield has a 64-byte
   * hardware receive buffer, and this library has a soft receive buffer. If this function returns
   * 64 + SOFT_BUFFER_SIZE_BYTES you may not be consuming data fast enough, and data could be lost.
   */
  int available() override;

  /**
   * @brief Read a byte from the receive buffer.
   *
   * @return The next byte from the receive FIFO. A value of -1 indicates that the buffer is empty.
   */
  int read() override;

  /**
   * @brief Peek at the next byte in the receive buffer without consuming it.
   *
   * @return The next byte that will be returned by read(). A value of -1 indicates that the buffer
   * is empty.
   */
  int peek(void) override;

  /**
   * @brief Wait until all data in the transmit buffer has been sent.
   */
  void flush(void) override;

  /**
   * @brief Write a byte to the transmit buffer.
   *
   * @param data The byte to output.
   *
   * @return The number of bytes written (always 1).
   */
  size_t write(uint8_t data) override;

  private:
  int16_t setBaudrate(uint32_t baudrate);
  uint8_t readRegister(uint8_t reg_addr);
  void writeRegister(uint8_t reg_addr, uint8_t val);
  void setLcr();
  void resetDevice(void);
  void fifoEnable(uint8_t fifo_enable);
  uint8_t fifoAvailableData(void);
  uint8_t fifoAvailableSpace(void);
  void writeByte(uint8_t val);
  void readBytes(char buf[], uint8_t len);

  uint8_t _i2cAddress;
  /* Points to first valid character in buffer */
  uint8_t _softBufferPtr = SOFT_BUFFER_SIZE_BYTES;
  char _softBuffer[SOFT_BUFFER_SIZE_BYTES];
};

} // namespace Zaber

#endif /* _ZABERSHIELD_H_ */
