#include "ZaberResult.h"

using namespace Zaber;

Result::Result(ErrorType error) : _error(error) {}

Result::Result(String text) : _text(text)
{
  switch (text.charAt(0))
  {
  case '@':
  {
    _type = REPLY;
    if (getIsRejected())
    {
      _error = COMMAND_REJECTED;
    }
    break;
  }
  case '#':
    _type = INFO;
    break;
  case '!':
    _type = ALERT;
    break;
  default:
  {
    _error = MESSAGE_INVALID;
    return;
  }
  }

  int16_t checksum = getChecksum();
  if (checksum >= 0)
  {
    int8_t checksumIndex = text.indexOf(':');
    uint8_t sum = checksum;
    for (int i = 1; i < checksumIndex; i++)
    {
      sum += text.charAt(i);
    }
    if (sum != 0)
    {
      _error = MESSAGE_INVALID;
      return;
    }
  }
}

Result::ErrorType Result::getError() { return _error; }

String Result::getErrorString()
{
  switch (_error)
  {
  case OK:
    return F("OK");
  case DEVICE_UNSPECIFIED:
    return F("Device Unspecified");
  case TIMEOUT:
    return F("Timeout");
  case MESSAGE_INVALID:
    return F("Message Invalid");
  case COMMAND_REJECTED:
    return F("Command Rejected");
  case NO_MESSAGE:
    return F("No Message");
  }
  return F("");
}

Result::MessageType Result::getType() { return _type; }

String Result::getText() { return _text; }

uint8_t Result::getDevice() { return atoi(_text.c_str() + 1); }

uint8_t Result::getAxis() { return _text.charAt(4) - '0'; }

int8_t Result::getId()
{
  if (_type == REPLY || _type == INFO)
  {
    return atoi(_text.c_str() + 6);
  }
  else
  {
    return -1;
  }
}

bool Result::getIsRejected()
{
  if (_type == REPLY)
  {
    return _text.charAt(9) == 'R';
  }
  else
  {
    return false;
  }
}

Result::IdleStatus Result::getStatus()
{
  if (_type != REPLY && _type != ALERT)
  {
    return IdleStatus::ERROR;
  }

  uint8_t index = _type == REPLY ? 12 : 6;
  if (_text.charAt(index) == 'I')
  {
    return IdleStatus::IDLE;
  }
  else if (_text.charAt(index) == 'B')
  {
    return IdleStatus::BUSY;
  }
  else
  {
    return IdleStatus::ERROR;
  }
}

bool Result::getHasWarning()
{
  if (_type == REPLY)
  {
    return _text.charAt(17) != '-';
  }
  else if (_type == ALERT)
  {
    return _text.charAt(11) != '-';
  }
  else
  {
    return false;
  }
}

String Result::getWarning()
{
  if (_type == REPLY)
  {
    return _text.substring(17, 19);
  }
  else if (_type == ALERT)
  {
    return _text.substring(11, 13);
  }
  else
  {
    return "--";
  }
}

String Result::getData()
{
  int8_t dataIndex;
  if (_type == REPLY)
  {
    dataIndex = 20;
  }
  else if (_type == INFO)
  {
    dataIndex = 9;
  }
  else if (_type == ALERT)
  {
    dataIndex = 14;
  }
  else
  {
    return "";
  }

  int8_t checksumIndex = _text.indexOf(':');
  if (checksumIndex >= 0)
  {
    if (dataIndex >= checksumIndex)
    {
      return "";
    }
    else
    {
      return _text.substring(dataIndex, checksumIndex);
    }
  }
  else
  {
    return _text.substring(dataIndex);
  }
}

int32_t Result::getDataInt()
{
  char *end;
  int32_t dataInt = strtol(getData().c_str(), &end, 10);
  if (end == getData().c_str())
  {
    return -1;
  }
  else
  {
    return dataInt;
  }
}

double Result::getDataDouble() { return getData().toDouble(); }

int16_t Result::getChecksum()
{
  int8_t checksumIndex = _text.indexOf(':');
  if (checksumIndex < 0)
  {
    return -1;
  }
  else
  {
    return strtol(_text.c_str() + checksumIndex + 1, NULL, HEX);
  }
}