#include "ZaberConnection.h"

#define MAX_MESSAGE_ID 99

using namespace Zaber;

Connection::Connection(SerialPort &serial) : _serialPort(serial)
{
  _serialPort.setTimeout(SERIAL_TIMEOUT);
}

Result Connection::genericCommand(const String &command, uint8_t device, uint8_t axis)
{
  if (device == 0)
  {
    return Result(Result::DEVICE_UNSPECIFIED);
  }

  uint8_t id = genericCommandNoResponse(command, device, axis);

  return waitForMessage(id, REPLY, _timeout);
}

MultiResult Connection::genericCommandMultiResponse(const String &command, uint8_t device,
                                                    uint8_t axis)
{
  uint8_t id = genericCommandNoResponse(command, device, axis);

  /* Send extra command to get final reponse from the device after all responses */
  if (device > 0)
  {
    genericCommandNoResponse("", device, axis, id);
  }

  return MultiResult(waitForMessage(id, REPLY, _timeout), *this, id, device == 0);
}

uint8_t Connection::genericCommandNoResponse(const String &command, uint8_t device, uint8_t axis,
                                             uint8_t id)
{
  if (id > 99)
  {
    id = getNextId();
  }

  /* Maximum command prefix is 8 chars + terminator: `01 1 99 \0`*/
  const uint8_t commandBufferLength = command.length() + 9;
  char commandBuffer[commandBufferLength];
  /* Empty Commands */
  if (command.length() == 0 || (command.length() == 1 && command[0] == '/'))
  {
    snprintf(commandBuffer, commandBufferLength, "%d %d %d", device, axis, id);
  }
  /* Commands with / as the starting character */
  else if (command[0] == '/')
  {
    snprintf(commandBuffer, commandBufferLength, "%d %d %d %s", device, axis, id,
             command.c_str() + 1);
  }
  /* Commands without / as the starting character */
  else
  {
    snprintf(commandBuffer, commandBufferLength, "%d %d %d %s", device, axis, id, command.c_str());
  }

  _serialPort.print('/');
  _serialPort.print(commandBuffer);
  if (_checksum)
  {
    char checksumBuffer[4] = {};
    snprintf(checksumBuffer, 4, ":%02X", generateChecksum(commandBuffer));
    _serialPort.println(checksumBuffer);
  }
  else
  {
    _serialPort.println();
  }

  return id;
}

Result Connection::waitForResponse() { return waitForResponse(255); }

Result Connection::waitForResponse(uint8_t messageId)
{
  return waitForMessage(messageId, REPLY_INFO, _timeout);
}

Result Connection::waitForAlert(uint32_t timeout) { return waitForMessage(255, ALERT, timeout); }

Result Connection::waitUntilIdle(uint8_t device, uint8_t axis, uint32_t timeout)
{
  if (device == 0)
  {
    return Result(Result::DEVICE_UNSPECIFIED);
  }

  uint32_t startOfWait = millis();
  while (timeout == 0 || millis() - startOfWait <= timeout)
  {
    Result result = checkForMessage(255, ALERT);
    if (result.getError() == Result::OK && result.getStatus() == Result::IDLE &&
        result.getDevice() == device && result.getAxis() == axis)
    {
      return result;
    }
    result = genericCommand("", device, axis);
    if (result.getError() == Result::OK && result.getStatus() == Result::IDLE)
    {
      return result;
    }
  }

  return Result(Result::TIMEOUT);
}

Result::IdleStatus Connection::getStatus(uint8_t device, uint8_t axis)
{
  if (device == 0)
  {
    return Result::ERROR;
  }

  Result result = genericCommand("", device, axis);
  if (result.getError() == Result::OK)
  {
    return result.getStatus();
  }
  else
  {
    return Result::ERROR;
  }
}

void Connection::setTimeout(uint32_t timeout) { _timeout = timeout; }

void Connection::setAlertCallback(AlertCallbackPtr callback) { _alertCallback = callback; }

void Connection::checkAlerts()
{
  Result result = checkForMessage(255, ALERT);
  while (result.getError() == Result::OK)
  {
    notifyAlert(result);
    result = checkForMessage(255, ALERT);
  }
}

void Connection::enableChecksum() { _checksum = true; }

void Connection::disableChecksum() { _checksum = false; }

void Connection::notifyAlert(Result result)
{
  if (_alertCallback != NULL)
  {
    _alertCallback(result);
  }
}

uint8_t Connection::getNextId()
{
  if (_nextMessageId > MAX_MESSAGE_ID)
  {
    _nextMessageId = 0;
    return 0;
  }
  else
  {
    return _nextMessageId++;
  }
}

Result Connection::waitForMessage(uint8_t messageId, CheckForMessageTypes types, uint32_t timeout)
{
  uint32_t startOfWait = millis();
  while (millis() - startOfWait <= timeout)
  {
    Result result = checkForMessage(messageId, types);
    if (result.getError() != Result::NO_MESSAGE)
    {
      return result;
    }
  }

  return Result(Result::TIMEOUT);
}

Result Connection::checkForMessage(uint8_t messageId, CheckForMessageTypes types)
{
  if (_serialPort.available())
  {
    Result result = Result(_serialPort.readStringUntil('\n'));
    if (result.getError() == Result::OK || result.getError() == Result::COMMAND_REJECTED)
    {
      if (result.getType() == Result::REPLY && (types & REPLY) &&
          (messageId > MAX_MESSAGE_ID || result.getId() == messageId))
      {
        return result;
      }
      else if (result.getType() == Result::INFO && (types & INFO) &&
               (messageId > MAX_MESSAGE_ID || result.getId() == messageId))
      {
        return result;
      }
      else if (result.getType() == Result::ALERT)
      {
        if (types & ALERT)
        {
          return result;
        }
        else
        {
          notifyAlert(result);
        }
      }
    }
    else if (result.getError() == Result::MESSAGE_INVALID)
    {
      return result;
    }
  }

  return Result(Result::NO_MESSAGE);
}

uint8_t Connection::generateChecksum(const char *command)
{
  uint8_t sum = 0;
  for (uint8_t i = 0; command[i] != '\0'; i++)
  {
    sum += command[i];
  }
  return (sum ^ 0xFF) + 1;
}