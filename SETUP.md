# Setup

**The setup instructions in this document are for library development only and are not required to use the Arduino library. To get started with the Arduino library, please see `README.md`.**

* Install the [PlatformIO extension](https://platformio.org/) for Visual Studio Code
* Install the PlatformIO CLI with `pip install platformio`

## Building
* Ensure you are on the `Default` or `build` environment on the [PlatformIO Toolbar](https://docs.platformio.org/en/stable/integration/ide/vscode.html#platformio-toolbar) at the bottom of the Visual Studio Code window.
* Press the [build button](https://docs.platformio.org/en/latest/_images/platformio-ide-vscode-build-project.png) (checkmark icon) on the [PlatformIO Toolbar](https://docs.platformio.org/en/stable/integration/ide/vscode.html#platformio-toolbar).

OR

* Run the following command: `pio run`

## Running Tests
* Switch to the `test` environment on the [PlatformIO Toolbar](https://docs.platformio.org/en/stable/integration/ide/vscode.html#platformio-toolbar) at the bottom of the Visual Studio Code window.
* Press the test button (beaker icon) on the [PlatformIO Toolbar](https://docs.platformio.org/en/stable/integration/ide/vscode.html#platformio-toolbar).

OR

* Run the following command: `pio test -e test`

## Building API Documentation
* Install the [doxygenmd](https://github.com/d99kris/doxygenmd) tool if it's not already installed on your system.
* Run the following command: `pio run -t build-api-docs`

## Building Examples
* Run the following command: `pio run -t build-examples`

## Releasing

The Arduino Library registry release requirements are [listed here](https://github.com/arduino/library-registry/blob/main/FAQ.md#updates). Publishing to the Arduino Library Registry using the following instructions will also publish to the PlatformIO Registry.

* Create a new release branch
* Ensure you have built the API documentation based on the latest changes (see above)
* Update the `CHANGELOG.md` file with any new changes for this release.
* Update the version number in the `library.properties` file.
* Push the release branch
* Create a new tag with `git tag vX.X.X` for the version number.
* Push the tag with `git push origin vX.X.X`
* Merge the release branch to `master`. The Arduino Library Registry automatically updates to the latest version within 1 hour.
    * Arduino Library Registry logs for this library are available [here](https://downloads.arduino.cc/libraries/logs/gitlab.com/zaber-core-libs/zaber-ascii-for-arduino/).