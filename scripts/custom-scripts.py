import os

Import("env")

BUILD_EXAMPLES_COMMAND_PREFIX = "pio ci -c platformio.ini -e examples -l src examples/"

env.AddCustomTarget(
    name="build-examples",
    dependencies=None,
    actions=[BUILD_EXAMPLES_COMMAND_PREFIX + dir for dir in os.listdir("examples")],
    title="Build Examples",
    description="Build all files in examples directory."
)

env.AddCustomTarget(
    name="build-api-docs",
    dependencies=None,
    actions=["doxygenmd src docs", "rm docs/README.md"],
    title="Build API Docs",
    description="Build Markdown documentation files for all header files."
)