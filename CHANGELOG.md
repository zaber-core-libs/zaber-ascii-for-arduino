# Changelog

## v2.0.1 (2024-04)

* Removes library dependency that was causing library installation error on Arduino IDE.

## v2.0.0 (2024-04)

*Note that this is a major release, and contains changes that will not be compatible with existing code that uses this library. Please continue to use version 1.X.X of the library if you do not wish to update your code to take advantage of this new functionality.*

* BREAKING CHANGES:
  * Moved all Zaber classes to new `Zaber` namespace.
  * Added new `Zaber::Result` class for handling response to a command.
  * Added new `Zaber::MultiResult` class for handling multiple responses to one command.
  * Renamed `ZaberAscii` class to `Zaber::Connection`.
    * Replaced `send()` functions with `genericCommand()`, `genericCommandMultiResponse()`, and `genericCommandNoResponse()` functions with added functionality.
    * Replaced `receive()` function with `waitForResponse()` and `waitForAlert()` functions with added functionality.
    * Added improved functionality for handling alerts.
    * Added user-configurable timeouts.
  * Added new `Zaber::Command` class to make it easier to build command from variables of different types.
  * Updates all examples to be compatible with new library changes, and to highlight new functionality.
  * Updates all documentation, adding more detail.
    * Adds a new user guide.
    * Adds API documentation for all classes and their functions.
  * Added tests.

## v.1.3.1 (2024-03)
* Fixed a bug where a ZaberShield object would receive a single byte at a time when instantiated as a Stream.

## v.1.3.0 (2024-02)
* Increased ZaberShield library message read performance by querying multiple bytes at once. An
  infinite amount of data can now be streamed from Zaber devices, subject to microcontroller speed
  limits.

## v1.2.0 (2019-04)

* Added support for the Zaber shield. The new ZaberShield class can be used instead of Serial
  to communicate with devices using the Zaber shield.
* Added an isIdle() method that tells you if a device has stopped moving without blocking program
  execution until the device does stop moving.
* Moved the getting started documentation from the Zaber wiki to local markdown format.
* Added a new example program, "Track Manual Moves", that demonstrates how to process multiple responses
  to a broadcast command and how to avoid blocking your program while waiting for a response.
* Added a new example program, "2D Scan", that demonstrates how to control two axes to visit locations
  on a grid pattern.

## v1.1.2 (2019-02)

* Fixed send() helper methods only accepting a 16-bit data value; they now take 32-bit input for the data payload.

## v1.1.1 (2018-09)

* Fixed an infinite loop bug in pollUntilIdle().
* Removed some superfluous include statements that caused problems under Linux.
* Improved examples to demonstrate consumption of devices' replies to commands; not consuming the
  replies can lead to serial port receive buffer overflow and corruption of subsequent replies.

## v1.1 (2018-05)

* Updated link to user guide.

## v1.0 (2018-03)

* Initial release