#include <gtest/gtest.h>
#include <ZaberCommand.h>

using namespace Zaber;

/* Creates command with string */
TEST(ZaberCommandTest, String)
{
  Command command("test");
  EXPECT_STREQ(((const String&)command).c_str(), "test");
}

/* Creates command with string and integer */
TEST(ZaberCommandTest, StringInteger)
{
  Command command("test", 42);
  EXPECT_STREQ(((const String&)command).c_str(), "test 42");
}

/* Creates command with string and float */
TEST(ZaberCommandTest, StringFloat)
{
  Command command("test", 1.02);
  EXPECT_STREQ(((const String&)command).c_str(), "test 1.02");
}

/* Creates command with string and string */
TEST(ZaberCommandTest, StringString)
{
  Command command("test", "second test");
  EXPECT_STREQ(((const String&)command).c_str(), "test second test");
}

/* Creates command with string and multiple args */
TEST(ZaberCommandTest, StringMultipleArgs)
{
  Command command("test", 1.02, 5, "test");
  EXPECT_STREQ(((const String&)command).c_str(), "test 1.02 5 test");
}