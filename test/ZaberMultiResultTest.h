#include <gtest/gtest.h>

#include <ZaberConnection.h>
#include <ZaberMultiResult.h>

using namespace fakeit;
using namespace Zaber;

ShieldMock *shield;
Connection *connection;

unsigned long millisTest = 0;

class ZaberMultiResultTest : public testing::Test
{
  protected:
  /* This is run before each message */
  void SetUp() override
  {
    /* Mock millis() to increment by 1 every call */
    When(Method(ArduinoFake(), millis)).AlwaysDo([](...) { return millisTest++; });

    /* Create new shield and connection objects */
    shield = new ShieldMock();
    connection = new Connection(*shield);

    /* Set the connection timeout to 20ms to reduce test times */
    connection->setTimeout(5);
  }
};

/* Multi-result is created with no additional messages */
TEST_F(ZaberMultiResultTest, NextFalse)
{
  shield->testResponses.push("@01 1 22 OK IDLE -- 0");

  MultiResult result(Result("@01 1 22 OK IDLE -- 0"), *connection, 22, false);
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);

  EXPECT_FALSE(result.next());
}

/* Multi-result is created with no additional messages from broadcast */
TEST_F(ZaberMultiResultTest, NextFalseBroadcast)
{
  MultiResult result(Result("@01 0 99 OK IDLE -- I'm device 1"), *connection, 99, true);
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 0);
  EXPECT_EQ(result.getId(), 99);
  EXPECT_EQ(result.getData(), "I'm device 1");

  EXPECT_FALSE(result.next());
}

/* Multi-result is created with one additional message from a device */
TEST_F(ZaberMultiResultTest, NextTrueOnce)
{
  shield->testResponses.push("#01 1 11 test");
  shield->testResponses.push("@01 1 11 OK IDLE -- 0");

  MultiResult result(Result("@01 1 11 OK IDLE -- 0"), *connection, 11, false);
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);

  EXPECT_TRUE(result.next());

  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::INFO);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 1);
  EXPECT_EQ(result.getId(), 11);
  EXPECT_EQ(result.getData(), "test");

  EXPECT_FALSE(result.next());
}

/* Multi-result is created with multiple additional messages from a device */
TEST_F(ZaberMultiResultTest, NextTrueMany)
{
  shield->testResponses.push("#01 1 11 test");
  shield->testResponses.push("#01 1 11 test2");
  shield->testResponses.push("@01 1 11 OK IDLE -- 0");

  MultiResult result(Result("@01 1 11 OK IDLE -- 0"), *connection, 11, false);
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);

  EXPECT_TRUE(result.next());

  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::INFO);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 1);
  EXPECT_EQ(result.getId(), 11);
  EXPECT_EQ(result.getData(), "test");

  EXPECT_TRUE(result.next());

  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::INFO);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 1);
  EXPECT_EQ(result.getId(), 11);
  EXPECT_EQ(result.getData(), "test2");

  EXPECT_FALSE(result.next());
}

/* Invalid message in multi-result chain doesn't interrupt chain */
TEST_F(ZaberMultiResultTest, NextTrueManyError)
{
  shield->testResponses.push("#01 1 11 test");
  shield->testResponses.push("////");
  shield->testResponses.push("#01 1 11 test2");
  shield->testResponses.push("@01 1 11 OK IDLE -- 0");

  MultiResult result(Result("@01 1 11 OK IDLE -- 0"), *connection, 11, false);
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);

  EXPECT_TRUE(result.next());

  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::INFO);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 1);
  EXPECT_EQ(result.getId(), 11);
  EXPECT_EQ(result.getData(), "test");

  EXPECT_TRUE(result.next());

  EXPECT_EQ(result.getError(), Result::MESSAGE_INVALID);
  EXPECT_EQ(result.getText(), "////");

  EXPECT_TRUE(result.next());

  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::INFO);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 1);
  EXPECT_EQ(result.getId(), 11);
  EXPECT_EQ(result.getData(), "test2");

  EXPECT_FALSE(result.next());
}

/* Unrelated message in multi-result chain doesn't interrupt chain */
TEST_F(ZaberMultiResultTest, NextTrueManyUnrelated)
{
  shield->testResponses.push("#01 1 11 test");
  shield->testResponses.push("#02 2 22 unrelated");
  shield->testResponses.push("#01 1 11 test2");
  shield->testResponses.push("@01 1 11 OK IDLE -- 0");

  MultiResult result(Result("@01 1 11 OK IDLE -- 0"), *connection, 11, false);
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);

  EXPECT_TRUE(result.next());

  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::INFO);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 1);
  EXPECT_EQ(result.getId(), 11);
  EXPECT_EQ(result.getData(), "test");

  EXPECT_TRUE(result.next());

  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::INFO);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 1);
  EXPECT_EQ(result.getId(), 11);
  EXPECT_EQ(result.getData(), "test2");

  EXPECT_FALSE(result.next());
}

/* Multi-result is created with multiple additional messages from broadcast */
TEST_F(ZaberMultiResultTest, NextTrueBroadcast)
{
  shield->testResponses.push("@02 0 99 OK IDLE -- I'm device 2");
  shield->testResponses.push("@03 0 99 OK IDLE -- I'm device 3");

  MultiResult result(Result("@01 0 99 OK IDLE -- I'm device 1"), *connection, 99, true);
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 0);
  EXPECT_EQ(result.getId(), 99);
  EXPECT_EQ(result.getData(), "I'm device 1");

  EXPECT_TRUE(result.next());

  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 2);
  EXPECT_EQ(result.getAxis(), 0);
  EXPECT_EQ(result.getId(), 99);
  EXPECT_EQ(result.getData(), "I'm device 2");

  EXPECT_TRUE(result.next());

  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 3);
  EXPECT_EQ(result.getAxis(), 0);
  EXPECT_EQ(result.getId(), 99);
  EXPECT_EQ(result.getData(), "I'm device 3");

  EXPECT_FALSE(result.next());
}