#include <gtest/gtest.h>

#include <ZaberResult.h>

using namespace Zaber;

/* Empty result text creates empty result */
TEST(ZaberResultTest, Empty)
{
  Result result("");
  EXPECT_EQ(result.getText(), "");
  EXPECT_EQ(result.getError(), Result::MESSAGE_INVALID);
  EXPECT_EQ(result.getType(), Result::EMPTY);
}

/* Invalid result text creates empty result */
TEST(ZaberResultTest, Invalid)
{
  Result result("/");
  EXPECT_EQ(result.getError(), Result::MESSAGE_INVALID);
  EXPECT_EQ(result.getType(), Result::EMPTY);
}

/* Valid reply result is parsed correctly */
TEST(ZaberResultTest, Reply)
{
  Result result("@01 2 42 OK IDLE -- 0");
  EXPECT_EQ(result.getText(), "@01 2 42 OK IDLE -- 0");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 2);
  EXPECT_EQ(result.getId(), 42);
  EXPECT_EQ(result.getIsRejected(), false);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
  EXPECT_EQ(result.getHasWarning(), false);
  EXPECT_EQ(result.getData(), "0");
  EXPECT_EQ(result.getDataInt(), 0);
  EXPECT_EQ(result.getDataDouble(), 0);
  EXPECT_EQ(result.getChecksum(), -1);
}

/* Rejected reply result is parsed correctly */
TEST(ZaberResultTest, ReplyRejected)
{
  Result result("@01 2 42 RJ IDLE -- BADCOMMAND");
  EXPECT_EQ(result.getError(), Result::COMMAND_REJECTED);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 2);
  EXPECT_EQ(result.getId(), 42);
  EXPECT_EQ(result.getIsRejected(), true);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
  EXPECT_EQ(result.getHasWarning(), false);
  EXPECT_EQ(result.getData(), "BADCOMMAND");
  EXPECT_EQ(result.getDataInt(), -1);
  EXPECT_EQ(result.getDataDouble(), 0);
}

/* Busy reply result is parsed correctly */
TEST(ZaberResultTest, ReplyBusy)
{
  Result result("@02 3 42 OK BUSY -- 0");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 2);
  EXPECT_EQ(result.getAxis(), 3);
  EXPECT_EQ(result.getId(), 42);
  EXPECT_EQ(result.getIsRejected(), false);
  EXPECT_EQ(result.getStatus(), Result::BUSY);
  EXPECT_EQ(result.getHasWarning(), false);
  EXPECT_EQ(result.getData(), "0");
  EXPECT_EQ(result.getDataInt(), 0);
  EXPECT_EQ(result.getDataDouble(), 0);
}

/* Reply result with warning is parsed correctly */
TEST(ZaberResultTest, ReplyWarning)
{
  Result result("@02 3 42 OK BUSY WR 0");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 2);
  EXPECT_EQ(result.getAxis(), 3);
  EXPECT_EQ(result.getId(), 42);
  EXPECT_EQ(result.getIsRejected(), false);
  EXPECT_EQ(result.getStatus(), Result::BUSY);
  EXPECT_EQ(result.getHasWarning(), true);
  EXPECT_EQ(result.getWarning(), "WR");
  EXPECT_EQ(result.getData(), "0");
  EXPECT_EQ(result.getDataInt(), 0);
}

/* Reply result with string data is parsed correctly */
TEST(ZaberResultTest, ReplyDataString)
{
  Result result("@02 3 00 OK IDLE -- Some Data");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 2);
  EXPECT_EQ(result.getAxis(), 3);
  EXPECT_EQ(result.getId(), 00);
  EXPECT_EQ(result.getIsRejected(), false);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
  EXPECT_EQ(result.getHasWarning(), false);
  EXPECT_EQ(result.getWarning(), "--");
  EXPECT_EQ(result.getData(), "Some Data");
  EXPECT_EQ(result.getDataInt(), -1);
  EXPECT_EQ(result.getDataDouble(), 0);
}

/* Reply data with negative integer is parsed correctly */
TEST(ZaberResultTest, ReplyDataInt)
{
  Result result("@02 3 00 OK IDLE -- -65535");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 2);
  EXPECT_EQ(result.getAxis(), 3);
  EXPECT_EQ(result.getId(), 00);
  EXPECT_EQ(result.getIsRejected(), false);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
  EXPECT_EQ(result.getHasWarning(), false);
  EXPECT_EQ(result.getWarning(), "--");
  EXPECT_EQ(result.getData(), "-65535");
  EXPECT_EQ(result.getDataInt(), -65535);
  EXPECT_EQ(result.getDataDouble(), -65535);
}

/* Reply data with integer and string is parsed correctly */
TEST(ZaberResultTest, ReplyDataIntPartial)
{
  Result result("@02 3 00 OK IDLE -- 12 Some Data");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 2);
  EXPECT_EQ(result.getAxis(), 3);
  EXPECT_EQ(result.getId(), 00);
  EXPECT_EQ(result.getIsRejected(), false);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
  EXPECT_EQ(result.getHasWarning(), false);
  EXPECT_EQ(result.getWarning(), "--");
  EXPECT_EQ(result.getData(), "12 Some Data");
  EXPECT_EQ(result.getDataInt(), 12);
  EXPECT_EQ(result.getDataDouble(), 12);
}

/* Reply data with float is parsed correctly */
TEST(ZaberResultTest, ReplyDataFloat)
{
  Result result("@02 3 00 OK IDLE -- 12.219");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 2);
  EXPECT_EQ(result.getAxis(), 3);
  EXPECT_EQ(result.getId(), 00);
  EXPECT_EQ(result.getIsRejected(), false);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
  EXPECT_EQ(result.getHasWarning(), false);
  EXPECT_EQ(result.getWarning(), "--");
  EXPECT_EQ(result.getData(), "12.219");
  EXPECT_EQ(result.getDataInt(), 12);
  EXPECT_EQ(result.getDataDouble(), 12.219);
}

/* Valid info result is parsed correctly */
TEST(ZaberResultTest, Info)
{
  Result result("#03 0 11 Some Data");
  EXPECT_EQ(result.getText(), "#03 0 11 Some Data");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::INFO);
  EXPECT_EQ(result.getDevice(), 3);
  EXPECT_EQ(result.getAxis(), 0);
  EXPECT_EQ(result.getId(), 11);
  EXPECT_EQ(result.getIsRejected(), false);
  EXPECT_EQ(result.getStatus(), Result::ERROR);
  EXPECT_EQ(result.getHasWarning(), false);
  EXPECT_EQ(result.getData(), "Some Data");
  EXPECT_EQ(result.getDataInt(), -1);
  EXPECT_EQ(result.getChecksum(), -1);
}

/* Info result with integer data is parsed correctly */
TEST(ZaberResultTest, InfoDataInt)
{
  Result result("#00 0 99 1111111");
  EXPECT_EQ(result.getDevice(), 0);
  EXPECT_EQ(result.getAxis(), 0);
  EXPECT_EQ(result.getId(), 99);
  EXPECT_EQ(result.getData(), "1111111");
  EXPECT_EQ(result.getDataInt(), 1111111);
}

/* Valid alert result is parsed correctly */
TEST(ZaberResultTest, Alert)
{
  Result result("!01 1 IDLE --");
  EXPECT_EQ(result.getText(), "!01 1 IDLE --");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::ALERT);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 1);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
  EXPECT_EQ(result.getHasWarning(), false);
  EXPECT_EQ(result.getData(), "");
  EXPECT_EQ(result.getDataInt(), -1);
}

/* Alert result with busy status is parsed correctly */
TEST(ZaberResultTest, AlertBusy)
{
  Result result("!02 2 BUSY --");
  EXPECT_EQ(result.getDevice(), 2);
  EXPECT_EQ(result.getAxis(), 2);
  EXPECT_EQ(result.getId(), -1);
  EXPECT_EQ(result.getStatus(), Result::BUSY);
  EXPECT_EQ(result.getHasWarning(), false);
  EXPECT_EQ(result.getData(), "");
  EXPECT_EQ(result.getDataInt(), -1);
}

/* Alert result with warning flag is parsed correctly */
TEST(ZaberResultTest, AlertWarning)
{
  Result result("!00 0 IDLE NC");
  EXPECT_EQ(result.getDevice(), 0);
  EXPECT_EQ(result.getAxis(), 0);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
  EXPECT_EQ(result.getHasWarning(), true);
  EXPECT_EQ(result.getWarning(), "NC");
  EXPECT_EQ(result.getData(), "");
  EXPECT_EQ(result.getDataInt(), -1);
}

/* Alert result with string data is parsed correctly */
TEST(ZaberResultTest, AlertData)
{
  Result result("!00 0 IDLE NC This is some data!");
  EXPECT_EQ(result.getDevice(), 0);
  EXPECT_EQ(result.getAxis(), 0);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
  EXPECT_EQ(result.getHasWarning(), true);
  EXPECT_EQ(result.getWarning(), "NC");
  EXPECT_EQ(result.getData(), "This is some data!");
  EXPECT_EQ(result.getDataInt(), -1);
}

/* Alert result with integer data is parsed correctly */
TEST(ZaberResultTest, AlertDataInt)
{
  Result result("!00 0 IDLE NC 00");
  EXPECT_EQ(result.getDevice(), 0);
  EXPECT_EQ(result.getAxis(), 0);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
  EXPECT_EQ(result.getHasWarning(), true);
  EXPECT_EQ(result.getWarning(), "NC");
  EXPECT_EQ(result.getData(), "00");
  EXPECT_EQ(result.getDataInt(), 0);
  EXPECT_EQ(result.getChecksum(), -1);
}

/* result with checksum is parsed correctly */
TEST(ZaberResultTest, Checksum)
{
  Result result("@01 1 02 OK IDLE -- Some Data:A8");
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 1);
  EXPECT_EQ(result.getId(), 2);
  EXPECT_FALSE(result.getIsRejected());
  EXPECT_EQ(result.getStatus(), Result::IDLE);
  EXPECT_FALSE(result.getHasWarning());
  EXPECT_EQ(result.getData(), "Some Data");
  EXPECT_EQ(result.getChecksum(), 0xA8);
}

/* result with single-digit checksum is parsed correctly */
TEST(ZaberResultTest, ChecksumSingleDigit)
{
  Result result("@01 1 01 OK IDLE -- 0:0B");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDataInt(), 0);
  EXPECT_EQ(result.getChecksum(), 0xB);
}

/* Alert with checksum and no data is parsed correctly */
TEST(ZaberResultTest, AlertChecksumNoData)
{
  Result result("!01 1 IDLE --:96");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::ALERT);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 1);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
  EXPECT_FALSE(result.getHasWarning());
  EXPECT_EQ(result.getData(), "");
  EXPECT_EQ(result.getDataInt(), -1);
  EXPECT_EQ(result.getChecksum(), 0x96);
}

/* Result with TIMEOUT error is created */
TEST(ZaberResultTest, StatusTimeout)
{
  Result result(Result::TIMEOUT);
  EXPECT_EQ(result.getError(), Result::TIMEOUT);
  EXPECT_EQ(result.getErrorString(), "Timeout");
}

/* Result with DEVICE_UNSPECIFIED error is created */
TEST(ZaberResultTest, StatusDeviceUnspecified)
{
  Result result(Result::DEVICE_UNSPECIFIED);
  EXPECT_EQ(result.getError(), Result::DEVICE_UNSPECIFIED);
  EXPECT_EQ(result.getErrorString(), "Device Unspecified");
}

/* Result with NO_MESSAGE error is created */
TEST(ZaberResultTest, StatusNoMessage)
{
  Result result(Result::NO_MESSAGE);
  EXPECT_EQ(result.getError(), Result::NO_MESSAGE);
  EXPECT_EQ(result.getErrorString(), "No Message");
}

/* Result with invalid message and MESSAGE_INVALID error is created */
TEST(ZaberResultTest, StatusMessageInvalid)
{
  Result result("adfds");
  EXPECT_EQ(result.getError(), Result::MESSAGE_INVALID);
  EXPECT_EQ(result.getErrorString(), "Message Invalid");
  EXPECT_EQ(result.getType(), Result::EMPTY);
}

/* Result with INVALID_MESSAGE error is created due to invalid checksum */
TEST(ZaberResultTest, ChecksumInvalid)
{
  Result result("@01 0 OK BUSY -- 0:69");
  EXPECT_EQ(result.getError(), Result::MESSAGE_INVALID);
}
