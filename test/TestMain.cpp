#ifdef ENV_TEST

#include <gtest/gtest.h>

#include "ZaberMultiResultTest.h"
#include "ZaberConnectionTest.h"
#include "ZaberResultTest.h"
#include "ZaberCommandTest.h"

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);

  /* Wrap in if statement to silence warning */
  if (RUN_ALL_TESTS())
    ;

  /* Always return zero-code and allow PlatformIO to parse results */
  return 0;
}

#endif /* ENV_TEST */
