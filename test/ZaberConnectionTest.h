#include <ArduinoFake.h>
#include <gtest/gtest.h>

#include <ZaberCommand.h>
#include <ZaberConnection.h>

#include "ZaberShieldMock.h"

using namespace fakeit;
using namespace Zaber;

unsigned int alertNotified = 0;

class ZaberConnectionTest : public testing::Test
{
  protected:
  /* This is run before each message */
  void SetUp() override
  {
    /* Mock millis() to increment by 1 every call */
    When(Method(ArduinoFake(), millis)).AlwaysDo([](...) { return millisTest++; });

    /* Create new shield and connection objects */
    shield = new ShieldMock();
    connection = new Connection(*shield);

    /* Set the connection timeout to 20ms to reduce test times */
    connection->setTimeout(5);

    /* Reset the alert notified counter*/
    alertNotified = 0;
  }
};

/*
  @brief Helper for genericCommand tests

  Sets up a response, sends a generic command, and checks the sent command.

  @param command          Command to send
  @param device           Device to send the command to
  @param axis             Axis to send the command to
  @param expectedCommand  Expected raw string of the sent command, must include \n on the end
  @param expectedResponse Expected raw string of the response

  @return The result of the command
*/
Result genericCommandTest(const char *command, uint8_t device, uint8_t axis,
                          const char *expectedCommand, const char *expectedResponse)
{
  shield->testResponses.push(expectedResponse);

  Result result = connection->genericCommand(command, device, axis);

  EXPECT_STREQ(shield->testCommands.front().c_str(), expectedCommand);
  shield->testCommands.pop();

  EXPECT_STREQ(result.getText().c_str(), expectedResponse);

  return result;
}

/* Empty command is sent as a / */
TEST_F(ZaberConnectionTest, GenericCommandEmpty)
{
  Result result = genericCommandTest("", 1, 1, "/1 1 0:2E\n", "@01 1 00 OK IDLE -- 0:0C");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getData(), "0");
}

/* Sending just a / does not append an extra / */
TEST_F(ZaberConnectionTest, GenericCommandEmptySlash)
{
  Result result = genericCommandTest("/", 1, 1, "/1 1 0:2E\n", "@01 1 00 OK IDLE -- 0:0C");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getData(), "0");
}

/* Simple command is formatted correctly */
TEST_F(ZaberConnectionTest, GenericCommandBasic)
{
  Result result = genericCommandTest("home", 2, 3, "/2 3 0 home:62\n", "@02 3 00 OK BUSY -- 0:E4");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getStatus(), Result::BUSY);
  EXPECT_EQ(result.getData(), "0");
}

/* Simple command with a / prefix is formatted correctly */
TEST_F(ZaberConnectionTest, GenericCommandBasicSlash)
{
  Result result = genericCommandTest("/home", 2, 3, "/2 3 0 home:62\n", "@02 3 00 OK BUSY -- 0:E4");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getStatus(), Result::BUSY);
  EXPECT_EQ(result.getData(), "0");
}

/* Command with no device sends no command to the device and returns a DEVICE_UNSPECIFIED result */
TEST_F(ZaberConnectionTest, GenericCommandNoDevice)
{
  Result result = connection->genericCommand("/home", 0, 1);
  EXPECT_EQ(result.getError(), Result::DEVICE_UNSPECIFIED);
  EXPECT_TRUE(shield->testCommands.empty());
}

/* Command with device and no axis is formatted correctly */
TEST_F(ZaberConnectionTest, GenericCommandNoAxis)
{
  shield->testResponses.push("@01 0 00 OK IDLE -- 1:0C");
  Result result = connection->genericCommand("get system.access", 1);

  EXPECT_STREQ(shield->testCommands.front().c_str(), "/1 0 0 get system.access:6A\n");

  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 0);
  EXPECT_EQ(result.getDataInt(), 1);
}

/* Command with device and no axis successfully receives a reply */
TEST_F(ZaberConnectionTest, GenericCommandAllAxes)
{
  Result result = genericCommandTest("home", 1, 0, "/1 0 0 home:66\n", "@01 0 00 OK BUSY -- 0:E8");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 0);
}

/* Multiple single commands in series are formatted correctly */
TEST_F(ZaberConnectionTest, GenericCommandSeries)
{
  Result result =
      genericCommandTest("warnings", 1, 0, "/1 0 0 warnings:A6\n", "@01 0 00 OK IDLE WL 01 WL:D0");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_TRUE(result.getHasWarning());
  EXPECT_EQ(result.getWarning(), "WL");
  EXPECT_EQ(result.getData(), "01 WL");

  result = genericCommandTest("warnings clear", 1, 0, "/1 0 1 warnings clear:7E\n",
                              "@01 0 01 OK IDLE -- 01 WL:18");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_FALSE(result.getHasWarning());
  EXPECT_EQ(result.getData(), "01 WL");

  result = genericCommandTest("home", 1, 0, "/1 0 2 home:64\n", "@01 0 02 OK BUSY -- 0:E6");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getId(), 2);
}

/* Sending 101 commands in series will cause ID to rollover from 99 to 0. Checksums disabled
 * correctly. */
TEST_F(ZaberConnectionTest, GenericCommandSeriesIdRollover)
{
  connection->disableChecksum();
  for (int i = 0; i < 100; i++)
  {
    String command = "/1 1 " + String(i) + "\n";
    String responseId = i < 10 ? "0" + String(i) : String(i);
    String response = "@01 0 " + responseId + " OK IDLE -- 0";
    Result result = genericCommandTest("", 1, 1, command.c_str(), response.c_str());
    EXPECT_EQ(result.getError(), Result::OK);
    EXPECT_EQ(result.getType(), Result::REPLY);
    EXPECT_EQ(result.getId(), i);
  }

  /* Expect 101st message to have ID of 0 */
  Result result = genericCommandTest("", 1, 1, "/1 1 0\n", "@01 0 00 OK IDLE 0");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getId(), 0);
}

/* Receiving rejected command reply is parsed correctly */
TEST_F(ZaberConnectionTest, GenericCommandRejected)
{
  Result result =
      genericCommandTest("move 0", 1, 1, "/1 1 0 move 0:07\n", "@01 1 00 RJ IDLE -- BADCOMMAND");
  EXPECT_EQ(result.getError(), Result::COMMAND_REJECTED);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_TRUE(result.getIsRejected());
  EXPECT_EQ(result.getData(), "BADCOMMAND");
}

/* Reply returned when there is an alert in the receive buffer */
TEST_F(ZaberConnectionTest, GenericCommandAlertInBuffer)
{
  shield->testResponses.push("!01 1 IDLE --");
  Result result =
      genericCommandTest("move min", 1, 1, "/1 1 0 move min:F3\n", "@01 1 00 OK BUSY -- 0");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getStatus(), Result::BUSY);
  EXPECT_EQ(result.getData(), "0");
}

/* Reply returned when there is an info message in the buffer */
TEST_F(ZaberConnectionTest, GenericCommandInfoInBuffer)
{
  shield->testResponses.push("#01 1 00 Some Message");
  Result result =
      genericCommandTest("move min", 1, 1, "/1 1 0 move min:F3\n", "@01 1 00 OK BUSY -- 0");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getStatus(), Result::BUSY);
  EXPECT_EQ(result.getData(), "0");
}

/* Enabling checksums appends checksum to command */
TEST_F(ZaberConnectionTest, GenericCommandChecksum)
{
  Result result = genericCommandTest("move abs 1000", 1, 0, "/1 0 0 move abs 1000:21\n",
                                     "@01 0 00 OK BUSY -- 0");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
}

/* Disabling checksums does not append checksum to command */
TEST_F(ZaberConnectionTest, GenericCommandChecksumDisable)
{
  connection->disableChecksum();
  Result result =
      genericCommandTest("move abs 1000", 1, 0, "/1 0 0 move abs 1000\n", "@01 0 00 OK BUSY -- 0");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
}

/* Re-enabling checksums appends checksum to command */
TEST_F(ZaberConnectionTest, GenericCommandChecksumReEnabled)
{
  connection->disableChecksum();
  connection->enableChecksum();
  Result result = genericCommandTest("move abs 1000", 1, 0, "/1 0 0 move abs 1000:21\n",
                                     "@01 0 00 OK BUSY -- 0");
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
}

/* Sends command with a ZaberCommand String */
TEST_F(ZaberConnectionTest, GenericCommandZaberCommand)
{
  shield->testResponses.push("@01 0 00 OK BUSY -- 0");
  Result result = connection->genericCommand(Command("move min"), 1, 1);

  EXPECT_STREQ(shield->testCommands.front().c_str(), "/1 1 0 move min:F3\n");

  EXPECT_EQ(result.getError(), Result::OK);
}

/* Sends command with a ZaberCommand String and Int */
TEST_F(ZaberConnectionTest, GenericCommandZaberCommandInt)
{
  shield->testResponses.push("@01 0 00 OK BUSY -- 0");
  Result result = connection->genericCommand(Command("move abs", 10000), 1, 1);

  EXPECT_STREQ(shield->testCommands.front().c_str(), "/1 1 0 move abs 10000:F0\n");

  EXPECT_EQ(result.getError(), Result::OK);
}

/* Returns invalid message if response is invalid */
TEST_F(ZaberConnectionTest, GenericCommandResponseInvalid)
{
  Result result =
      genericCommandTest("move abs 1000", 1, 0, "/1 0 0 move abs 1000:21\n", "@01 BUS -:89");
  EXPECT_EQ(result.getError(), Result::MESSAGE_INVALID);
}

/*
  @brief Helper for genericCommandMultiResponse tests

  Sets up responses, sends a generic command, and checks the sent command for the first response.

  @param command            Command to send
  @param device             Device to send the command to
  @param axis               Axis to send the command to
  @param expectedCommand    Expected raw string of the sent command, must include \n on the end
  @param expectedResponses  Expected raw string of the responses

  @return The result of the command
*/
MultiResult genericCommandMultiResponseTest(const char *command, uint8_t device, uint8_t axis,
                                            const char *expectedCommand,
                                            std::vector<const char *> expectedResponses)
{
  for (const char *expectedResponse : expectedResponses)
  {
    shield->testResponses.push(expectedResponse);
  }

  MultiResult result = connection->genericCommandMultiResponse(command, device, axis);

  EXPECT_STREQ(shield->testCommands.front().c_str(), expectedCommand);
  shield->testCommands.pop();

  EXPECT_STREQ(result.getText().c_str(), expectedResponses.front());

  return result;
}

/* Receiving a single response */
TEST_F(ZaberConnectionTest, GenericCommandMultiResponseSingle)
{
  MultiResult result = genericCommandMultiResponseTest(
      "home", 1, 1, "/1 1 0 home:65\n", {"@01 1 00 OK BUSY -- 0:E7", "@01 1 00 OK BUSY -- 0:E7"});
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getStatus(), Result::BUSY);
  EXPECT_EQ(result.getData(), "0");

  EXPECT_FALSE(result.next());
}

/* Receiving all responses for a single command to a specific device */
TEST_F(ZaberConnectionTest, GenericCommandMultiResponseMultiple)
{
  MultiResult result = genericCommandMultiResponseTest(
      "trigger print", 1, 0, "/1 0 0 trigger print:CE\n",
      {"@01 0 00 OK IDLE -- 0:0D", "#01 0 00 trigger 1:6A", "#01 0 00 when none:2D",
       "#01 0 00 action a none:E0", "#01 0 00 action b none:DF", "#01 0 00 disable:DB",
       "@01 0 00 OK IDLE -- 0:0D"});
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);

  EXPECT_TRUE(result.next());
  EXPECT_EQ(result.getType(), Result::INFO);
  EXPECT_EQ(result.getData(), "trigger 1");

  EXPECT_TRUE(result.next());
  EXPECT_EQ(result.getType(), Result::INFO);
  EXPECT_EQ(result.getData(), "when none");

  EXPECT_TRUE(result.next());
  EXPECT_EQ(result.getType(), Result::INFO);
  EXPECT_EQ(result.getData(), "action a none");

  EXPECT_TRUE(result.next());
  EXPECT_EQ(result.getType(), Result::INFO);
  EXPECT_EQ(result.getData(), "action b none");

  EXPECT_TRUE(result.next());
  EXPECT_EQ(result.getType(), Result::INFO);
  EXPECT_EQ(result.getData(), "disable");

  EXPECT_FALSE(result.next());
  EXPECT_FALSE(shield->available());
}

/* Receiving all responses for a broadcast command to all devices */
TEST_F(ZaberConnectionTest, GenericCommandMultiResponseAllDevices)
{
  MultiResult result = genericCommandMultiResponseTest(
      "set comm.alert 1", 0, 0, "/0 0 0 set comm.alert 1:61\n",
      {"@01 0 00 OK IDLE -- 0", "@02 0 00 OK IDLE -- 0", "@03 0 00 OK IDLE -- 0"});
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getDataInt(), 0);

  EXPECT_TRUE(result.next());
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 2);
  EXPECT_EQ(result.getDataInt(), 0);

  EXPECT_TRUE(result.next());
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 3);
  EXPECT_EQ(result.getDataInt(), 0);

  EXPECT_FALSE(result.next());
  EXPECT_FALSE(shield->available());
}

/* Sending a single command with no response */
TEST_F(ZaberConnectionTest, GenericCommandNoResponse)
{
  uint8_t id = connection->genericCommandNoResponse("home", 1, 1);
  EXPECT_EQ(id, 0);
  EXPECT_STREQ(shield->testCommands.front().c_str(), "/1 1 0 home:65\n");
  EXPECT_FALSE(shield->available());
}

/* Sending a single command with no response and custom ID */
TEST_F(ZaberConnectionTest, GenericCommandNoResponseCustomId)
{
  uint8_t id = connection->genericCommandNoResponse("home", 1, 1, 24);
  EXPECT_EQ(id, 24);
  EXPECT_STREQ(shield->testCommands.front().c_str(), "/1 1 24 home:2F\n");
  EXPECT_FALSE(shield->available());
}

/* Sending a single command with no response and invalid ID sends default ID */
TEST_F(ZaberConnectionTest, GenericCommandNoResponseInvalidId)
{
  uint8_t id = connection->genericCommandNoResponse("home", 1, 1, 100);
  EXPECT_EQ(id, 0);
  EXPECT_STREQ(shield->testCommands.front().c_str(), "/1 1 0 home:65\n");
  EXPECT_FALSE(shield->available());
}

/* Sending a single command and leaving response in the receive buffer */
TEST_F(ZaberConnectionTest, GenericCommandNoResponseReply)
{
  shield->testResponses.push("@01 1 00 OK IDLE -- 42");
  uint8_t id = connection->genericCommandNoResponse("get pos", 1, 1);
  EXPECT_STREQ(shield->testCommands.front().c_str(), "/1 1 0 get pos:5C\n");
  EXPECT_TRUE(shield->available());
}

/* Waiting for a response gets reply from receive buffer */
TEST_F(ZaberConnectionTest, WaitForResponseReply)
{
  uint8_t id = connection->genericCommandNoResponse("home", 1, 1);
  EXPECT_EQ(id, 0);

  shield->testResponses.push("@01 1 00 OK BUSY -- 0:E7");

  Result result = connection->waitForResponse();
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getStatus(), Result::BUSY);
  EXPECT_EQ(result.getData(), "0");
}

/* Waiting for a response gets info message from receive buffer */
TEST_F(ZaberConnectionTest, WaitForResponseInfo)
{
  shield->testResponses.push("@01 0 00 OK IDLE -- 0");
  shield->testResponses.push("#01 0 00 trigger 1");
  connection->genericCommand("trigger print", 1, 0);

  Result result = connection->waitForResponse();
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::INFO);
  EXPECT_EQ(result.getData(), "trigger 1");
}

/* Waiting for a response with a specific ID gets reply with that ID out of the buffer, discards
 * other response */
TEST_F(ZaberConnectionTest, WaitForResponseCorrectId)
{
  uint8_t id = connection->genericCommandNoResponse("home", 1, 1);
  shield->testResponses.push("@01 1 99 OK BUSY -- 2");
  shield->testResponses.push("@01 1 00 OK BUSY -- 3");

  Result result = connection->waitForResponse(id);
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getStatus(), Result::BUSY);
  EXPECT_EQ(result.getDataInt(), 3);
}

/* Waiting for alert gets alert out of the buffer, discards other messages */
TEST_F(ZaberConnectionTest, WaitForAlert)
{
  shield->testResponses.push("@01 1 00 OK BUSY -- 0:E7");
  shield->testResponses.push("!01 1 IDLE --:96");

  Result result = connection->waitForAlert();
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::ALERT);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
  EXPECT_EQ(result.getData(), "");
}

/* Queries device status until device replies IDLE */
TEST_F(ZaberConnectionTest, WaitUntilIdleReply)
{
  /* Alerts are inserted to make tests work, as the test environment requires replies to be queued
   * ahead of time */
  shield->testResponses.push("!02 1 BUSY --");
  shield->testResponses.push("@02 1 00 OK BUSY -- 0");
  shield->testResponses.push("!02 1 BUSY --");
  shield->testResponses.push("@02 1 01 OK BUSY -- 0");
  shield->testResponses.push("!02 1 BUSY --");
  shield->testResponses.push("@02 1 02 OK BUSY -- 0");
  shield->testResponses.push("!02 1 BUSY --");
  shield->testResponses.push("@02 1 03 OK IDLE -- 0");

  Result result = connection->waitUntilIdle(2, 1);
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 2);
  EXPECT_EQ(result.getAxis(), 1);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
  EXPECT_EQ(result.getDataInt(), 0);
}

/* Idle alert for the correct device/axis in the receive buffer causes waitUntilIdle to return
 * immediately
 */
TEST_F(ZaberConnectionTest, WaitUntilIdleAlertAtStart)
{
  shield->testResponses.push("!01 1 IDLE --:96");

  Result result = connection->waitUntilIdle(1, 1);
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::ALERT);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 1);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
}

/* Idle alert after busy responses causes waitUntilIdle to return */
TEST_F(ZaberConnectionTest, WaitUntilIdleAlertAfterReply)
{
  shield->testResponses.push("!02 1 BUSY --");
  shield->testResponses.push("@02 1 00 OK BUSY -- 0");
  shield->testResponses.push("!02 1 BUSY --");
  shield->testResponses.push("@02 1 01 OK BUSY -- 0");
  shield->testResponses.push("!02 1 IDLE --");

  Result result = connection->waitUntilIdle(2, 1);
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::ALERT);
  EXPECT_EQ(result.getDevice(), 2);
  EXPECT_EQ(result.getAxis(), 1);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
}

/* Idle alert for different device doesn't cause waitUntilIdle to return */
TEST_F(ZaberConnectionTest, WaitUntilIdleAlertWrongDevice)
{
  shield->testResponses.push("!02 0 IDLE --");
  shield->testResponses.push("@01 0 00 OK IDLE -- 0");

  Result result = connection->waitUntilIdle(1, 0);
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 0);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
  EXPECT_EQ(result.getDataInt(), 0);
  EXPECT_FALSE(shield->available());
}

/* Idle alert for different axis doesn't cause waitUntilIdle to return */
TEST_F(ZaberConnectionTest, WaitUntilIdleAlertWrongAxis)
{
  shield->testResponses.push("!02 2 IDLE --");
  shield->testResponses.push("@02 1 00 OK IDLE -- 0");

  Result result = connection->waitUntilIdle(2, 1);
  EXPECT_EQ(result.getError(), Result::OK);
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_EQ(result.getDevice(), 2);
  EXPECT_EQ(result.getAxis(), 1);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
  EXPECT_EQ(result.getDataInt(), 0);
  EXPECT_FALSE(shield->available());
}

/* waitUntilIdle returns error for device 0 */
TEST_F(ZaberConnectionTest, WaitUntilIdleDeviceUnspecified)
{
  shield->testResponses.push("!01 1 IDLE --");

  Result result = connection->waitUntilIdle(0, 0);
  EXPECT_EQ(result.getError(), Result::DEVICE_UNSPECIFIED);
  EXPECT_TRUE(shield->available());
}

/* Reaches timeout before IDLE status received */
TEST_F(ZaberConnectionTest, WaitUntilIdleTimeout)
{
  /* Alerts are inserted to make tests work, as the test environment requires replies to be queued
   * ahead of time */
  shield->testResponses.push("!02 1 BUSY --");
  shield->testResponses.push("@02 1 00 OK BUSY -- 0");
  shield->testResponses.push("!02 1 BUSY --");
  shield->testResponses.push("@02 1 01 OK BUSY -- 0");
  shield->testResponses.push("!02 1 BUSY --");
  shield->testResponses.push("@02 1 02 OK BUSY -- 0");
  shield->testResponses.push("!02 1 BUSY --");
  shield->testResponses.push("@02 1 03 IDLE -- 0");

  Result result = connection->waitUntilIdle(2, 1, 2);
  EXPECT_EQ(result.getError(), Result::TIMEOUT);
  EXPECT_TRUE(shield->available());
}

/* getStatus returns busy when device responds with BUSY */
TEST_F(ZaberConnectionTest, GetStatusBusy)
{
  shield->testResponses.push("@01 1 00 OK BUSY -- 0");
  EXPECT_EQ(connection->getStatus(1, 1), Result::BUSY);
}

/* getStatus returns idle when device responds with idle */
TEST_F(ZaberConnectionTest, GetStatusIdle)
{
  shield->testResponses.push("@01 0 00 OK IDLE -- 0");
  EXPECT_EQ(connection->getStatus(1, 1), Result::IDLE);
}

/* getStatus returns error when device unspecified */
TEST_F(ZaberConnectionTest, GetStatusNoDevice)
{
  EXPECT_EQ(connection->getStatus(0, 0), Result::ERROR);
}

/* getStatus returns error when response invalid */
TEST_F(ZaberConnectionTest, GetStatusInvalidResponse)
{
  shield->testResponses.push(" 01 0 00 OK IDLE -- 0");
  EXPECT_EQ(connection->getStatus(1, 1), Result::ERROR);
}

/*
  @brief Alert callback helper

  Helper that checks message is alert, for device 1, axis 2, is idle, and doesn't have warning. Also
  increments alert notified buffer.

  @param message  Received alert message
*/
void alertCallback(Result result)
{
  alertNotified++;
  EXPECT_EQ(result.getType(), Result::ALERT);
  EXPECT_EQ(result.getDevice(), 1);
  EXPECT_EQ(result.getAxis(), 2);
  EXPECT_EQ(result.getStatus(), Result::IDLE);
  EXPECT_FALSE(result.getHasWarning());
}

/* Alert callback is triggered when alerts are checked */
TEST_F(ZaberConnectionTest, CheckAlert)
{
  connection->setAlertCallback(alertCallback);
  shield->testResponses.push("!01 2 IDLE --");
  connection->checkAlerts();
  EXPECT_EQ(alertNotified, 1);
}

/* Alert callback is triggered 3 times for 3 alerts in the buffer when alerts are checked once */
TEST_F(ZaberConnectionTest, CheckAlertMultiple)
{
  connection->setAlertCallback(alertCallback);
  shield->testResponses.push("!01 2 IDLE --");
  shield->testResponses.push("!01 2 IDLE --");
  shield->testResponses.push("!01 2 IDLE --");
  connection->checkAlerts();
  EXPECT_EQ(alertNotified, 3);
}

/* Alert callback is triggered for 1 of 3 messages in buffer that is an alert */
TEST_F(ZaberConnectionTest, CheckAlertOthersInBuffer)
{
  connection->setAlertCallback(alertCallback);
  shield->testResponses.push("!01 2 IDLE --");
  shield->testResponses.push("@02 1 0 OK BUSY -- 0");
  shield->testResponses.push("!01 2 IDLE --");
  connection->checkAlerts();
  EXPECT_EQ(alertNotified, 1);
  EXPECT_TRUE(shield->available());
}

/* Program doesn't crash when alerts are checked without an alert callback set */
TEST_F(ZaberConnectionTest, CheckAlertNotSet)
{
  shield->testResponses.push("!01 2 IDLE --");
  connection->checkAlerts();
  EXPECT_EQ(alertNotified, 0);
  EXPECT_FALSE(shield->available());
}

/* Alert in the buffer when a generic command is sent causes alert callback to be notified */
TEST_F(ZaberConnectionTest, NotifyAlertDuringGenericCommand)
{
  connection->setAlertCallback(alertCallback);
  shield->testResponses.push("!01 2 IDLE --");

  Result result = genericCommandTest("home", 2, 3, "/2 3 0 home:62\n", "@02 3 00 OK BUSY -- 0");
  EXPECT_EQ(result.getType(), Result::REPLY);

  EXPECT_EQ(alertNotified, 1);
  EXPECT_FALSE(shield->available());
}

/* Alerts in the buffer when a generic command is sent causes alert callback to be notified
 * twice */
TEST_F(ZaberConnectionTest, NotifyAlertsDuringGenericCommand)
{
  connection->setAlertCallback(alertCallback);
  shield->testResponses.push("!01 2 IDLE --");
  shield->testResponses.push("!01 2 IDLE --");

  Result result = genericCommandTest("home", 2, 3, "/2 3 0 home:62\n", "@02 3 00 OK BUSY -- 0");
  EXPECT_EQ(result.getType(), Result::REPLY);

  EXPECT_EQ(alertNotified, 2);
  EXPECT_FALSE(shield->available());
}

/* Alerts in the buffer when a generic command with multi response is sent causes alert
 * callback to be notified twice */
TEST_F(ZaberConnectionTest, NotifyAlertsDuringGenericCommandMultiple)
{
  connection->setAlertCallback(alertCallback);
  shield->testResponses.push("!01 2 IDLE --");
  shield->testResponses.push("!01 2 IDLE --");

  MultiResult result = genericCommandMultiResponseTest(
      "home", 1, 1, "/1 1 0 home:65\n", {"@01 1 00 OK BUSY -- 0", "@01 1 00 OK BUSY -- 0"});
  EXPECT_EQ(result.getType(), Result::REPLY);
  EXPECT_FALSE(result.next());

  EXPECT_EQ(alertNotified, 2);
  EXPECT_FALSE(shield->available());
}

/* Alerts in the buffer when waiting for response causes alert callback to be notified twice */
TEST_F(ZaberConnectionTest, NotifyAlertsDuringWaitForResponse)
{
  connection->setAlertCallback(alertCallback);
  shield->testResponses.push("!01 2 IDLE --");
  shield->testResponses.push("!01 2 IDLE --");
  shield->testResponses.push("@01 1 00 OK BUSY -- 0");

  Result result = connection->waitForResponse();
  EXPECT_EQ(result.getType(), Result::REPLY);

  EXPECT_EQ(alertNotified, 2);
  EXPECT_FALSE(shield->available());
}

/* Alerts in the buffer when waiting for alert does not cause alert callback to be notified */
TEST_F(ZaberConnectionTest, NotifyAlertNotDuringWaitForAlert)
{
  connection->setAlertCallback(alertCallback);
  shield->testResponses.push("!01 2 IDLE --");
  shield->testResponses.push("!01 2 IDLE --");

  Result result = connection->waitForAlert();
  EXPECT_EQ(result.getType(), Result::ALERT);

  EXPECT_EQ(alertNotified, 0);
  EXPECT_TRUE(shield->available());
}
