#ifndef _ZABER_SHIELD_MOCK_H_
#define _ZABER_SHIELD_MOCK_H_

#include <Arduino.h>
#include <queue>

namespace Zaber
{

class ShieldMock
{
  public:
  /* Test Variables*/
  std::queue<String> testCommands;
  std::queue<String> testResponses;

  /* Stubs */
  void begin(uint32_t baudrate = 115200) {}
  void setTimeout(unsigned long timeout) {}

  /* Mock Implementations */
  int available() { return !testResponses.empty(); }

  String readStringUntil(char terminator)
  {
    if (available())
    {
      String top = testResponses.front();
      testResponses.pop();
      return top;
    }
    else
    {
      return "";
    }
  }

  /* Mock Implementations of Inherited Functions */
  void print(const char *text)
  {
    if (testCommands.empty())
    {
      testCommands.push(text);
    }
    else if (testCommands.back().endsWith("\n"))
    {
      testCommands.push(text);
    }
    else
    {
      testCommands.back().concat(text);
    }
  }

  void print(String text) { print(text.c_str()); }

  void print(unsigned char number) { print(String(number)); }

  void print(char character) { print(String(character)); }

  void println() { print('\n'); }

  void println(String text) { print(text + '\n'); }

  void println(unsigned char number) { println(String(number)); }

  void println(unsigned char number, int base) { println(String(number, base)); }

  private:
};

} // namespace Zaber

#endif /* _ZABER_SHIELD_MOCK_H_ */
