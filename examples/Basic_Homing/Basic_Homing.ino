/*
  Basic_Homing.ino

  Basic example for controlling Zaber devices in the ASCII protocol.

  Sends a Zaber device to its home position on startup.
*/

#include <ZaberShield.h>
#include <ZaberConnection.h>

using namespace Zaber;

Shield shield(ZABERSHIELD_ADDRESS_AA);
Connection connection(shield);

void setup()
{
  /* Initialize Zaber ASCII shield */
  shield.begin();

  /* Issue a home command to device 1 */
  connection.genericCommand("home", 1);
}

void loop() {}
