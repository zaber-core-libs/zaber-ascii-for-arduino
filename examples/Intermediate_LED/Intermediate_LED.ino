/*
  Intermediate_LED.ino

  Intermediate example for controlling Zaber devices in the ASCII protocol.

  Queries for the Zaber device's position on each loop, and disables the Zaber device's
  LED power/status lights if the device is in the home position. Otherwise, the lights
  are enabled.
*/

#include <ZaberShield.h>
#include <ZaberConnection.h>

using namespace Zaber;

Shield shield(ZABERSHIELD_ADDRESS_AA);
Connection connection(shield);

void setup()
{
  /* Initialize Zaber shield */
  shield.begin();
}

void loop()
{
  /* Issue a get position command to the device and save the result */
  Result result = connection.genericCommand("get pos", 1);

  /* Check that the result of the response is OK */
  if (result.getError() == Result::OK)
  {
    if (result.getDataInt() == 0)
    {
      /* Set the device's LED lights to OFF if it is at home */
      connection.genericCommand("set system.led.enable 0", 1);
    }
    else
    {
      /* Set the device's LED lights to ON */
      connection.genericCommand("set system.led.enable 1", 1);
    }
  }
}
