/*
  Track_Manual_Moves.ino

  Example that queries position from a zaber device and outputs it over the Serial monitor.

  This example periodically queries the position from a Zaber device and outputs it over the Serial
  monitor. Any other messages that may be received from a device are also output.
*/

#include <ZaberShield.h>
#include <ZaberConnection.h>

using namespace Zaber;

Shield shield(ZABERSHIELD_ADDRESS_AA);
Connection connection(shield);

/*
  This function will be passed as a callback function to the connection, so that it is called
  whenever an alert is received from a device. This allows alerts to be handled semi-asynchronously.
*/
void alertReceived(Result alert)
{
  if (alert.getError() == Result::OK)
  {
    Serial.print("*** Received an alert: ");
    Serial.println(alert.getText());
  }
}

void setup()
{
  shield.begin();

  /*
    This example uses the serial monitor for output and a Zaber shield for device interfacing at the
    same time. This combination makes debugging easier.
  */
  Serial.begin(9600);
  /*
    Wait for a USB connection to be established to a PC.
  */
  while (!Serial)
  {
  }
  delay(100);

  Serial.println("Starting...");

  /* Set the alert callback to the function previously defined. */
  connection.setAlertCallback(alertReceived);

  /*
    Enable alerts on all connected devices. Since multiple devices may be connected, it must be
    sent as a multiResponse command, and the result must be checked until all responses have been
    cleared from the buffer.
  */
  MultiResult result = connection.genericCommandMultiResponse("set comm.alert 1");
  while (result.next())
  {
  }
}

void loop()
{
  /* Check the receive buffer for any alerts that may have been received. */
  connection.checkAlerts();

  /* Broadcast a command to all devices connected to the Zaber shield. */
  MultiResult result = connection.genericCommandMultiResponse("get pos");

  /* Process every reply that comes back. */
  do
  {
    if (result.getError() == Result::OK)
    {
      Serial.print("Position of device ");
      Serial.print(result.getDevice());
      Serial.print(": ");
      Serial.println(result.getData());
    }
    else
    {
      Serial.print("An error has occured: ");
      Serial.println(result.getErrorString());
      break;
    }

    /* Get the next response */
  } while (result.next());

  /*
    At this point every available received message has been processed, so we could do optionally do
    some other work while waiting for more to arrive.
  */
  delay(1);
}