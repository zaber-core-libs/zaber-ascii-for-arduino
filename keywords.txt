#######################################
# keywords.txt
#
# Allows for syntax-highlighting in the
# Arduino IDE
#######################################

# Datatypes (KEYWORD1)
Shield	KEYWORD1
Connection	KEYWORD1
Result	KEYWORD1
MultiResult	KEYWORD1
Command	KEYWORD1
ErrorType	KEYWORD1
MessageType	KEYWORD1


# Functions (KEYWORD2)
genericCommand	KEYWORD2
genericCommandMultiResponse	KEYWORD2
genericCommandNoResponse	KEYWORD2
waitForResponse	KEYWORD2
waitForAlert	KEYWORD2
waitUntilIdle	KEYWORD2
getStatus	KEYWORD2
setTimeout	KEYWORD2
setAlertCallback	KEYWORD2
checkAlerts	KEYWORD2
enableChecksum	KEYWORD2
disableChecksum	KEYWORD2
next	KEYWORD2
getError	KEYWORD2
getErrorString	KEYWORD2
getType	KEYWORD2
getText	KEYWORD2
getDevice	KEYWORD2
getAxis	KEYWORD2
getId	KEYWORD2
getIsRejected	KEYWORD2
getStatus	KEYWORD2
getHasWarning	KEYWORD2
getWarning	KEYWORD2
getData	KEYWORD2
getDataInt	KEYWORD2
getDataDouble	KEYWORD2
getChecksum	KEYWORD2
begin	KEYWORD2
end	KEYWORD2
available	KEYWORD2
read	KEYWORD2
peek	KEYWORD2
flush	KEYWORD2
write	KEYWORD2

# Constants (LITERAL1)
SOFT_BUFFER_SIZE_BYTES	LITERAL1
ZABERSHIELD_ADDRESS_AA	LITERAL1
ZABERSHIELD_ADDRESS_AB	LITERAL1
ZABERSHIELD_ADDRESS_BA	LITERAL1
ZABERSHIELD_ADDRESS_BB	LITERAL1
SERIAL_TIMEOUT	LITERAL1
