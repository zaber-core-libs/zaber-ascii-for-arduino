# Zaber ASCII Library for Arduino

The Zaber ASCII Library allows you to use an Arduino microcontroller to easily control your Zaber devices. Zaber devices are controlled using ASCII messages over an RS-232 serial connection. This library contains support for the [Zaber X-series Shield](https://www.zaber.com/products/accessories/X-AS01), and this is the recommended way to connect an Arduino to a Zaber device.

This library supports the following Arduino devices:
- Arduino Uno R3
- Arduino Mega R3
- Arduino Uno R4 (Recommended)

_This library may work with other Arduino / non-Arduino boards, but has not been tested with them._

## Installation

This library is available in the [Arduino Library Manager](https://docs.arduino.cc/software/ide-v1/tutorials/installing-libraries/) in the [Arduino IDE](https://www.arduino.cc/en/Main/Software). It's also available in the [PlatformIO Registry](https://registry.platformio.org/libraries/gitlab-zaber-core-libs/Zaber%20ASCII) for the [PlatformIO IDE](https://platformio.org/platformio-ide).

## Documentation

### How-To Guides

- [Quick Start Guide](docs/Quick%20Start.md) - How to get started running a minimal example on an Arduino.
- [Arduino Beginner's Guide](docs/Arduino%20Beginner's%20Guide.md) - Detailed code examples and troubleshooting steps for those new to Arduino.
- [User Guide](docs/User%20Guide.md) - Instructions and code snippets for common library functionality.

### API Reference

Documentation for each class in this library, including all of their functions:
* [`Zaber::Connection`](docs/Zaber_Connection.md)
* [`Zaber::Result`](docs/Zaber_Result.md)
* [`Zaber::MultiResult`](docs/Zaber_MultiResult.md)
* [`Zaber::Command`](docs/Zaber_Command.md)
* [`Zaber::Shield`](docs/Zaber_Shield.md)

### Example Code

Several example Arduino sketches are located in the `examples/` folder. Once the library has been installed, the examples can be loaded into Arduino IDE by going to `File -> Examples -> Zaber ASCII` and selecting one of the available options.

## Looking for Binary Protocol support?

Zaber devices use two protocols: ASCII and Binary. This library only supports the newer ASCII protocol. If you would prefer to use the older Binary protocol, there is a separate library also available through the Arduino IDE Library Manager, or [here](https://gitlab.com/zaber-core-libs/zaber-binary-for-arduino#zaber-binary-library-for-arduino).