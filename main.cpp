/*
  This file is here only to build the library for development with PlatformIO. It is not necessary
  when building an Arduino project with this library.
*/

#include "ZaberConnection.h"
#include "ZaberShield.h"

using namespace Zaber;

Shield shield(ZABERSHIELD_ADDRESS_AA);
Connection connection(shield);

void setup() { shield.begin(); }

void loop() {}
